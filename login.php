<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">


    <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
</head>

<?php
    include 'koneksi.php';
?>


<body class="bg-color-1">
    <div>
        <form class="form" name="login" action="" method="post">
            <div class="align-middle">
                <a href="#">
                    <center><img class="my-sm-3" src="gambar/logomitra10.svg" width="250"></center>
                </a>
                <div class="tengah kotak_tengah2">
                    <center><img id="profile-img" width="100" class="profile-img-card rounded-circle filter-blue" src="gambar/useraccount.svg" /></center>
                    </br>
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <span class="input-group-text warna-icon-login"><i class="fas fa-user"></i></span>
                        </div>
                            <input type="text" name="username" class="form-control input_user border-list" placeholder="Username">
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-append">
                            <span class="input-group-text warna-icon-login"><i class="fas fa-key"></i></span>
                        </div>
                            <input type="password" name="password" class="form-control input_pass border-list" placeholder="Password">
                    </div>
                    <a href="#" class="text-decoration">Forgot Password?</a>
                    </br>

                    </br>

                    <table width="100%">
                        <tr>
                            <td style="border: 0; padding: 0px;">
                                <a href="index.php" class="btn btn-danger button-left button-space">&nbsp;Back&nbsp;</a>
                                <button type="submit" name="submit" class="btn btn-primary button-right">Sign In</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>

    <?php

    if (isset($_POST['submit'])) {
        $username = $_POST['username'];
        $password = md5(trim($_POST['password']));

        $strSQL = "SELECT id, id_level, username, password FROM tb_user WHERE username='$username' AND password='$password'";
        $login = mysqli_query($koneksi, $strSQL) or die ("Query Salah!!!");
        $cek = mysqli_fetch_array($login);

        $strSQL2 = "SELECT username, password FROM tb_user";
        $login2 = mysqli_query($koneksi, $strSQL2) or die ("Query Salah!!!");
        while ($cek2 = mysqli_fetch_array($login2)) {
            $cekpass = $cek2['password'];
            $cekuser = $cek2['username'];
        

        if (empty($_POST['password']) && empty($_POST['username'])) {
            echo "<script>alert('Silahkan masukan Username & Password!');history.go(-1)</script>";
        }elseif (empty($_POST['username'])) {
            echo "<script>alert('Silahkan masukan Username!');history.go(-1)</script>";
        }elseif (empty($_POST['password'])) {
            echo "<script>alert('Silahkan masukan Password!');history.go(-1)</script>";
        }elseif ($username != $cekuser) {
            echo "<script>alert('Username yang anda masukan salah!');window.location='login.php'; </script>";
        }elseif ($password != $cekpass) {
            echo "<script>alert('Password yang anda masukan salah!');window.location='login.php'; </script>";
        }elseif ($cek > 0) {
            session_start();
            if ($cek['id_level'] == "1") {
                $_SESSION['userlogin'] = $username;
                // $_SESSION['status'] = "login";
                header("location:homemaster.php");
            }elseif ($cek['id_level'] == "2") {
                $_SESSION['userlogin'] = $username;
                // $_SESSION['status'] = "login";
                header("location:home.php");
            }    
        }else{
            echo "<script>alert('Username atau Password yang anda masukan salah!');window.location='login.php'; </script>";
        }}
    }

    ?>

    <script src="assets/js/jquery.js"></script> 
    <script src="assets/js/popper.js"></script> 
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>