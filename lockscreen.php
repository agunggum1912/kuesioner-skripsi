<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">


    <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
</head>

<?php
 include 'koneksi.php';

    if (isset($_GET['username'])) {
        $userlock = $_GET['username'];
        session_start();
        session_unset();
        session_destroy();
    }else{
        session_start();
        if (!isset($_SESSION['userlogin'])) {
        // if($_SESSION['status'] != "login") {
        echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
        }
    }
?>


<body class="bg-color-1">
    <div>
        <form id="login-form" class="form" action="" method="post">
            <div class="align-middle">
                <a href="#">
                    <center><img class="my-sm-3" src="gambar/logomitra10.svg" width="250"></center>
                </a>
                <div class="tengah kotak_tengah2">
                    <div class="foto_profil">
                        <?php
                            $strSQL = "SELECT tb_user.id, tb_user.username, tb_user.nama, tb_user.foto FROM tb_user WHERE username='$userlock' ";
                            $query = mysqli_query ($koneksi, $strSQL) or die ("query salah");
                            while ($row = mysqli_fetch_array($query)){
                            $id = $row ['id'];

                            $cek_foto = $row ['foto'];
                            $tempat_foto = 'foto/'.$row['foto']; 
                            if ($cek_foto) {
                                echo "<center><img id='profile-img' width='100' height='100' class='profile-img-card rounded-circle margin-bottom-1' src='$tempat_foto'></center>"; 
                            }else{
                                echo "<img src='foto/blank.png'></a></br>";
                            }
                            echo "<span class='text-lockscreen-1'><center>".$row['nama']."</center></span>";
                        ?>
                    </div>
                    
                    <div class="input-group mb-2">
                        <div class="input-group-append">
                            <span class="input-group-text warna-icon-login"><i class="fas fa-key"></i></span>
                        </div>
                        <form name="login" method="post" action="">
                            <input type="hidden" name="username" value="<?php echo $row['username']; }?>">
                            <input type="password" name="password" class="form-control input_pass border-list" value="" placeholder="Password">
                            <div class="input-group-append">
                                <button name="submit" type="submit" class="btn warna-icon-login border-radius-1"><i class="fas fa-arrow-right"></i></button>
                            </div>
                        </form>
                    </div>
                    <span class="text-lockscreen-2"><center>Enter your password to retrieve your session<a href="logout.php" class="text-decoration-2"> Or sign in as a different user</a></center></span>
                    
                    </br>

                </div>
            </div>
        </form>
    </div>

    <?php
    
    if (isset($_POST['submit'])) {
        $username = $_POST['username'];
        $password = md5(trim($_POST['password']));

        $strSQLuser = "SELECT tb_user.username FROM tb_user";
        $loginuser = mysqli_query($koneksi, $strSQLuser) or die ("Query Salah!!!");
        $cekuser = mysqli_fetch_array($loginuser);

        $strSQLpass = "SELECT tb_user.password FROM tb_user";
        $loginpass = mysqli_query($koneksi, $strSQLpass) or die ("Query Salah!!!");
        $cekpass = mysqli_fetch_array($loginpass);
        
        $strSQL = "SELECT tb_user.id, tb_user.id_level, tb_user.username, tb_user.password FROM tb_user WHERE username='$username' AND password='$password'";
        $login = mysqli_query($koneksi, $strSQL) or die ("Query Salah!!!");
        $cek = mysqli_fetch_array($login);


        if (empty($_POST['password']) && empty($_POST['username'])) {
            echo "<script>alert('Silahkan masukan Username & Password!');history.go(-1)</script>";
        }elseif (empty($_POST['username'])) {
            echo "<script>alert('Silahkan masukan Username!');history.go(-1)</script>";
        }elseif (empty($_POST['password'])) {
            echo "<script>alert('Silahkan masukan Password!');history.go(-1)</script>";
        }elseif ($username != $cekuser['username']) {
            echo "<script>alert('Username yang anda masukan salah!');window.location='login.php'; </script>";
        }elseif ($password != $cekpass['password']) {
            echo "<script>alert('Password yang anda masukan salah!');window.location='login.php'; </script>";
        }elseif ($cek > 0) {
            session_start();
            if ($cek['id_level'] == "1") {
                $_SESSION['userlogin'] = $username;
                // $_SESSION['status'] = "login";
                header("location:homemaster.php");
            }elseif ($cek['id_level'] == "2") {
                $_SESSION['userlogin'] = $username;
                // $_SESSION['status'] = "login";
                header("location:home.php");
            }    
        }else{
            echo "<script>alert('Username atau Password yang anda masukan salah!');window.location='login.php'; </script>";
        }
    }

    ?>

    <script src="assets/js/jquery.js"></script> 
    <script src="assets/js/popper.js"></script> 
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>