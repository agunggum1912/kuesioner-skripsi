<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';
  include 'algoritma.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }

  $sql1 = "SELECT id, no_qb, time_steam FROM tb_kritik";
  $qry1 = mysqli_query ($koneksi, $sql1) or die ("query kritik salah");
                  
  $sqlpromotion = "SELECT * FROM tb_promotion";
  $qrypromotion = mysqli_query($koneksi, $sqlpromotion) or die ("query promotion salah");

  $sqlpeople = "SELECT * FROM tb_people";
  $qrypeople = mysqli_query($koneksi, $sqlpeople) or die ("query people salah");

  $sqlproduk = "SELECT * FROM tb_produk";
  $qryproduk = mysqli_query($koneksi, $sqlproduk) or die ("query produk salah");

  $sqlprice = "SELECT * FROM tb_price";
  $qryprice = mysqli_query($koneksi, $sqlprice) or die ("query price salah");

  $sqlplace = "SELECT * FROM tb_place";
  $qryplace = mysqli_query($koneksi, $sqlplace) or die ("query place salah");
  $no = 0;

  ?>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <?php
            $strSQL = "SELECT tb_user.id, tb_user.username, tb_user.nama, tb_user.foto FROM tb_user WHERE username='$_SESSION[userlogin]' ";
            $query = mysqli_query ($koneksi, $strSQL) or die ("query salah");
            while ($row = mysqli_fetch_array($query)){
            $id = $row ['id'];

            echo $row["username"];

          ?> 
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="reportresult.php" class="brand-link">
      <img src="gambar/logom10.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Mitra10 Q-Big</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row ['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="setting.php" class="d-block">
            <?php echo $row["nama"]; }?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="reportresult.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Report Result
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deleteuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="edituserid.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Manage Question
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="addquestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deletequestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="editquestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit Question</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="lockscreen.php?username=<?php echo $_SESSION['userlogin']; ?>" class="nav-link">
              <font style="color: #ed1c24;"><i class="nav-icon fas fa-user-lock"></i></font>
              <p>
                LockScreen
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="reportresult.php">Home</a></li>
              <li class="breadcrumb-item active">Report Result</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small card -->
          <div class="small-box bg-danger">
            <div class="inner">
              <?php
              echo "<h3>"."$jumlah"."</h3>";
              ?>

              <p>Comments</p>
            </div>
            <div class="icon">
              <i class="fas fa-comment"></i>
            </div>
            <a href="comment.php" class="small-box-footer">
              More info <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small card -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>
                <?php
                  echo $totalpersenkonversi; 
                ?>
                <sup style="font-size: 20px">%</sup>
              </h3>

              <p>All Score Rate</p>
            </div>
            <div class="icon">
              <i class="fas fa-chart-bar"></i>
            </div>
            <a href="allscore.php" class="small-box-footer">
              More info <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small card -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>Print</h3>

              <p>Report Result</p>
            </div>
            <div class="icon">
              <i class="fas fa-print"></i>
            </div>
            <a href="print.php" target="blank" class="small-box-footer">
              Print Report <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->


        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Table Report Questionnaire</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No QB</th>
                  <th>Score</th>
                  <th>Presentation</th>
                  <th>Date Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                  while ($data1 = mysqli_fetch_array($qry1)) {
                    $no++;
                    $datapromotion = mysqli_fetch_array($qrypromotion);
                    $datapeople = mysqli_fetch_array($qrypeople);
                    $dataproduk = mysqli_fetch_array($qryproduk);
                    $dataprice = mysqli_fetch_array($qryprice);
                    $dataplace = mysqli_fetch_array($qryplace);
                    $noqb = $data1['no_qb'];
                    $time = $data1['time_steam'];
                    $idkritik = $data1['id'];
                    $promotion1 = $datapromotion['j_promotion1'];
                    $promotion2 = $datapromotion['j_promotion2'];
                    $promotion3 = $datapromotion['j_promotion3'];
                    $promotion4 = $datapromotion['j_promotion4'];
                    $promotion5 = $datapromotion['j_promotion5'];
                    $promotion6 = $datapromotion['j_promotion6'];
                    $promotion7 = $datapromotion['j_promotion7'];
                    $promotion8 = $datapromotion['j_promotion8'];
                    $promotion9 = $datapromotion['j_promotion9'];
                    $promotion10 = $datapromotion['j_promotion10'];
                    $people1 = $datapeople['j_people1'];
                    $people2 = $datapeople['j_people2'];
                    $people3 = $datapeople['j_people3'];
                    $people4 = $datapeople['j_people4'];
                    $people5 = $datapeople['j_people5'];
                    $people6 = $datapeople['j_people6'];
                    $people7 = $datapeople['j_people7'];
                    $people8 = $datapeople['j_people8'];
                    $people9 = $datapeople['j_people9'];
                    $people10 = $datapeople['j_people10'];
                    $produk1 = $dataproduk['j_produk1'];
                    $produk2 = $dataproduk['j_produk2'];
                    $produk3 = $dataproduk['j_produk3'];
                    $produk4 = $dataproduk['j_produk4'];
                    $produk5 = $dataproduk['j_produk5'];
                    $produk6 = $dataproduk['j_produk6'];
                    $produk7 = $dataproduk['j_produk7'];
                    $produk8 = $dataproduk['j_produk8'];
                    $produk9 = $dataproduk['j_produk9'];
                    $produk10 = $dataproduk['j_produk10'];
                    $price1 = $dataprice['j_price1'];
                    $price2 = $dataprice['j_price2'];
                    $price3 = $dataprice['j_price3'];
                    $price4 = $dataprice['j_price4'];
                    $price5 = $dataprice['j_price5'];
                    $price6 = $dataprice['j_price6'];
                    $price7 = $dataprice['j_price7'];
                    $price8 = $dataprice['j_price8'];
                    $price9 = $dataprice['j_price9'];
                    $price10 = $dataprice['j_price10'];
                    $place1 = $dataplace['j_place1'];
                    $place2 = $dataplace['j_place2'];
                    $place3 = $dataplace['j_place3'];
                    $place4 = $dataplace['j_place4'];
                    $place5 = $dataplace['j_place5'];
                    $place6 = $dataplace['j_place6'];
                    $place7 = $dataplace['j_place7'];
                    $place8 = $dataplace['j_place8'];
                    $place9 = $dataplace['j_place9'];
                    $place10 = $dataplace['j_place10'];

                    if ($promotion1 != 0) {
                      $npromotion1 = 1;
                    }

                    if ($promotion2 != 0) {
                      $npromotion2 = 1;
                    }

                    if ($promotion3 != 0) {
                      $npromotion3 = 1;
                    }

                    if ($promotion4 != 0) {
                      $npromotion4 = 1;
                    }

                    if ($promotion5 != 0) {
                      $npromotion5 = 1;
                    }

                    if ($promotion6 != 0) {
                      $npromotion6 = 1;
                    }

                    if ($promotion7 != 0) {
                      $npromotion7 = 1;
                    }

                    if ($promotion8 != 0) {
                      $npromotion8 = 1;
                    }

                    if ($promotion9 != 0) {
                      $npromotion9 = 1;
                    }

                    if ($promotion10 != 0) {
                      $npromotion10 = 1;
                    }

                    if ($people1 != 0) {
                      $npeople1 = 1;
                    }

                    if ($people2 != 0) {
                      $npeople2 = 1;
                    }

                    if ($people3 != 0) {
                      $npeople3 = 1;
                    }

                    if ($people4 != 0) {
                      $npeople4 = 1;
                    }

                    if ($people5 != 0) {
                      $npeople5 = 1;
                    }

                    if ($people6 != 0) {
                      $npeople6 = 1;
                    }

                    if ($people7 != 0) {
                      $npeople7 = 1;
                    }

                    if ($people8 != 0) {
                      $npeople8 = 1;
                    }

                    if ($people9 != 0) {
                      $npeople9 = 1;
                    }

                    if ($people10 != 0) {
                      $npeople10 = 1;
                    }

                    if ($produk1 != 0) {
                      $nproduk1 = 1;
                    }

                    if ($produk2 != 0) {
                      $nproduk2 = 1;
                    }

                    if ($produk3 != 0) {
                      $nproduk3 = 1;
                    }

                    if ($produk4 != 0) {
                      $nproduk4 = 1;
                    }

                    if ($produk5 != 0) {
                      $nproduk5 = 1;
                    }

                    if ($produk6 != 0) {
                      $nproduk6 = 1;
                    }

                    if ($produk7 != 0) {
                      $nproduk7 = 1;
                    }

                    if ($produk8 != 0) {
                      $nproduk8 = 1;
                    }

                    if ($produk9 != 0) {
                      $nproduk9 = 1;
                    }

                    if ($produk10 != 0) {
                      $nproduk10 = 1;
                    }

                    if ($price1 != 0) {
                      $nprice1 = 1;
                    }

                    if ($price2 != 0) {
                      $nprice2 = 1;
                    }

                    if ($price3 != 0) {
                      $nprice3 = 1;
                    }

                    if ($price4 != 0) {
                      $nprice4 = 1;
                    }

                    if ($price5 != 0) {
                      $nprice5 = 1;
                    }

                    if ($price6 != 0) {
                      $nprice6 = 1;
                    }

                    if ($price7 != 0) {
                      $nprice7 = 1;
                    }

                    if ($price8 != 0) {
                      $nprice8 = 1;
                    }

                    if ($price9 != 0) {
                      $nprice9 = 1;
                    }

                    if ($price10 != 0) {
                      $nprice10 = 1;
                    }

                    if ($place1 != 0) {
                      $nplace1 = 1;
                    }

                    if ($place2 != 0) {
                      $nplace2 = 1;
                    }

                    if ($place3 != 0) {
                      $nplace3 = 1;
                    }

                    if ($place4 != 0) {
                      $nplace4 = 1;
                    }

                    if ($place5 != 0) {
                      $nplace5 = 1;
                    }

                    if ($place6 != 0) {
                      $nplace6 = 1;
                    }

                    if ($place7 != 0) {
                      $nplace7 = 1;
                    }

                    if ($place8 != 0) {
                      $nplace8 = 1;
                    }

                    if ($place9 != 0) {
                      $nplace9 = 1;
                    }

                    if ($place10 != 0) {
                      $nplace10 = 1;
                    }

                    $sum =$promotion1 + $promotion2 + $promotion3 + $promotion4 + $promotion5 + $promotion6 + $promotion7 + $promotion8 + $promotion9 + $promotion10 + $people1 + $people2 + $people3 + $people4 + $people5 + $people6 + $people7 + $people8 + $people9 + $people10 + $produk1 + $produk2 + $produk3 + $produk4 + $produk5 + $produk6 + $produk7 + $produk8 + $produk9 + $produk10 + $price1 + $price2 + $price3 + $price4 + $price5 + $price6 + $price7 + $price8 + $price9 + $price10 + $place1 + $place2 + $place3 + $place4 + $place5 + $place6 + $place7 + $place8 + $place9 + $place10;

                    $nilaipembagi = $npromotion1 + $npromotion2 + $npromotion3 + $npromotion4 + $npromotion5 + $npromotion6 + $npromotion7 + $npromotion8 + $npromotion9 + $npromotion10 + $npeople1 + $npeople2 + $npeople3 + $npeople4 + $npeople5 + $npeople6 + $npeople7 + $npeople8 + $npeople9 + $npeople10 + $nproduk1 + $nproduk2 + $nproduk3 + $nproduk4 + $nproduk5 + $nproduk6 + $nproduk7 + $nproduk8 + $nproduk9 + $nproduk10 + $nprice1 + $nprice2 + $nprice3 + $nprice4 + $nprice5 + $nprice6 + $nprice7 + $nprice8 + $nprice9 + $nprice10 + $nplace + $nplace1 + $nplace2 + $nplace3 + $nplace4 + $nplace5 + $nplace6 + $nplace7 + $nplace8 + $nplace9 + $nplace10;
                    $hasil = $sum / $nilaipembagi;

                    $hasilpersen = $hasil / 5 * 100;
                    $konversipersen = number_format($hasilpersen,1,",",".");

                    $hasilkonversi = number_format($hasil,1,",",".");
  
                ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $noqb; ?></td>
                  <td><?php echo $hasilkonversi; ?></td>
                  <td><?php echo $konversipersen;?>%</td>
                  <td><?php echo $time; ?></td>
                  <td width="8"><a href="detailhasilkuesioner.php?id=<?php echo $idkritik; ?>" class="btn btn-success float-right button-space"><i class='font-setting-4 far fa-eye'></i></a></td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No QB</th>
                  <th>Score</th>
                  <th>Presentation</th>
                  <th>Date Time</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
