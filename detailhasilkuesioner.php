<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }
  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

  $id = $_GET['id'];

  $sqlppeople = "SELECT * FROM  tb_pertanyaan_people";
  $qryppeople = mysqli_query($koneksi, $sqlppeople) or die ("Query Pertanyaan People salah!");
  while ($rowppeople = mysqli_fetch_array($qryppeople)) {
    $ppeople[] = $rowppeople['p_people'];
  }

  $sqlpeople = "SELECT * FROM tb_people WHERE id='$id'";
  $qrypeople = mysqli_query($koneksi, $sqlpeople) or die ("Query People salah!");

  $sqlpplace = "SELECT * FROM  tb_pertanyaan_place";
  $qrypplace = mysqli_query($koneksi, $sqlpplace) or die ("Query Pertanyaan Place salah!");
  while ($rowpplace = mysqli_fetch_array($qrypplace)) {
    $pplace[] = $rowpplace['p_place'];
  }

  $sqlplace = "SELECT * FROM tb_place WHERE id='$id'";
  $qryplace = mysqli_query($koneksi, $sqlplace) or die ("Query Place salah!");

  $sqlpprice = "SELECT * FROM  tb_pertanyaan_price";
  $qrypprice = mysqli_query($koneksi, $sqlpprice) or die ("Query Pertanyaan Price salah!");
  while ($rowpprice = mysqli_fetch_array($qrypprice)) {
    $pprice[] = $rowpprice['p_price'];
  }

  $sqlprice = "SELECT * FROM tb_price WHERE id='$id'";
  $qryprice = mysqli_query($koneksi, $sqlprice) or die ("Query Price salah!");

  $sqlpproduk = "SELECT * FROM  tb_pertanyaan_produk";
  $qrypproduk = mysqli_query($koneksi, $sqlpproduk) or die ("Query Pertanyaan Produk salah!");
  while ($rowpproduk = mysqli_fetch_array($qrypproduk)) {
    $pproduk[] = $rowpproduk['p_produk'];
  }

  $sqlproduk = "SELECT * FROM tb_produk WHERE id='$id'";
  $qryproduk = mysqli_query($koneksi, $sqlproduk) or die ("Query Produk salah!");

  $sqlppromotion = "SELECT * FROM  tb_pertanyaan_promotion";
  $qryppromotion = mysqli_query($koneksi, $sqlppromotion) or die ("Query Pertanyaan Promotion salah!");
  while ($rowppromotion = mysqli_fetch_array($qryppromotion)) {
    $ppromotion[] = $rowppromotion['p_promotion'];
  }

  $sqlpromotion = "SELECT * FROM tb_promotion WHERE id='$id'";
  $qrypromotion = mysqli_query($koneksi, $sqlpromotion) or die ("Query Promotion salah!");

?>
<style type="text/css">
  .red {
    color: red;
  }
</style>


</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <?php
            $strSQL = "SELECT tb_user.id, tb_user.username, tb_user.nama, tb_user.foto FROM tb_user WHERE username='$_SESSION[userlogin]' ";
            $query = mysqli_query ($koneksi, $strSQL) or die ("query salah");
            while ($row = mysqli_fetch_array($query)){
            $id = $row ['id'];

            echo $row["username"];

          ?> 
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="reportresult.php" class="brand-link">
      <img src="gambar/logom10.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Mitra10 Q-Big</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row ['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="setting.php" class="d-block">
            <?php echo $row["nama"]; }?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="reportresult.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Report Result
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deleteuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="edituserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Manage Question
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="addquestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deletequestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="editquestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit Question</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="lockscreen.php?username=<?php echo $_SESSION['userlogin']; ?>" class="nav-link">
              <font style="color: #ed1c24;"><i class="nav-icon fas fa-user-lock"></i></font>
              <p>
                LockScreen
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detail</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="reportresult.php">Home</a></li>
              <li class="breadcrumb-item"><a href="reportresult.php">Report Result</a></li>
              <li class="breadcrumb-item active">Detail</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">    
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header font-setting-3 bg-color-primary">
                <h3 class="card-title">Pertanyaan People</h3>
                <div></div>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus font-setting-4"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Question</center></th>
                      <th><center>Score</center></th>
                      <th><center>Result</center></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                      $no = array(1,2,3,4,5,6,7,8,9,10);
                      while ($rowpeople = mysqli_fetch_array($qrypeople)) {
                        if ($rowpeople['j_people1'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people1]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[0]; ?></center></td>
                      <td><?php if ($ppeople[0] != '') {
                        echo $ppeople[0];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people1']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people2'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people2]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[1]; ?></center></td>
                      <td><?php if ($ppeople[1] != '') {
                        echo $ppeople[1];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people2']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people3'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people3]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[2]; ?></center></td>
                      <td><?php if ($ppeople[2] != '') {
                        echo $ppeople[2];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people3']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people4'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people4]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[3]; ?></center></td>
                      <td><?php if ($ppeople[3] != '') {
                        echo $ppeople[3];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people4']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people5'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people5]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[4]; ?></center></td>
                      <td><?php if ($ppeople[4] != '') {
                        echo $ppeople[4];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people5']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people6'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people6]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[5]; ?></center></td>
                      <td><?php if ($ppeople[5] != '') {
                        echo $ppeople[5];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people6']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people7'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people7]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[6]; ?></center></td>
                      <td><?php if ($ppeople[6] != '') {
                        echo $ppeople[6];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people7']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people8'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people8]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[7]; ?></center></td>
                      <td><?php if ($ppeople[7] != '') {
                        echo $ppeople[7];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people8']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people9'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people9]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[8]; ?></center></td>
                      <td><?php if ($ppeople[8] != '') {
                        echo $ppeople[8];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people9']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpeople['j_people10'] != 0) {
                          $sqljwb1 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpeople[j_people10]'");
                          while ($rowjwb1 = mysqli_fetch_array($sqljwb1)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[9]; ?></center></td>
                      <td><?php if ($ppeople[9] != '') {
                        echo $ppeople[9];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpeople['j_people10']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb1['jawaban']; ?></center></td>
                    </tr>
                    <?php }}} ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header font-setting-3 bg-color-primary">
                <h3 class="card-title">Pertanyaan Place</h3>
                <div></div>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus font-setting-4"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Question</center></th>
                      <th><center>Score</center></th>
                      <th><center>Result</center></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                      $no = array(1,2,3,4,5,6,7,8,9,10);
                      while ($rowplace = mysqli_fetch_array($qryplace)) {
                        if ($rowplace['j_place1'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place1]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[0]; ?></center></td>
                      <td><?php if ($pplace[0] != '') {
                        echo $pplace[0];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place1']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place2'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place2]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[1]; ?></center></td>
                      <td><?php if ($pplace[1] != '') {
                        echo $pplace[1];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place2']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place3'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place3]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[2]; ?></center></td>
                      <td><?php if ($pplace[2] != '') {
                        echo $pplace[2];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place3']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place4'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place4]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[3]; ?></center></td>
                      <td><?php if ($pplace[3] != '') {
                        echo $pplace[3];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place4']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place5'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place5]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[4]; ?></center></td>
                      <td><?php if ($pplace[4] != '') {
                        echo $pplace[4];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place5']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place6'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place6]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[5]; ?></center></td>
                      <td><?php if ($pplace[5] != '') {
                        echo $pplace[5];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place6']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place7'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place7]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[6]; ?></center></td>
                      <td><?php if ($pplace[6] != '') {
                        echo $pplace[6];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place7']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place8'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place8]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[7]; ?></center></td>
                      <td><?php if ($pplace[7] != '') {
                        echo $pplace[7];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place8']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place9'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place9]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[8]; ?></center></td>
                      <td><?php if ($pplace[8] != '') {
                        echo $pplace[8];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place9']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowplace['j_place10'] != 0) {
                          $sqljwb2 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowplace[j_place10]'");
                          while ($rowjwb2 = mysqli_fetch_array($sqljwb2)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[9]; ?></center></td>
                      <td><?php if ($pplace[9] != '') {
                        echo $pplace[9];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowplace['j_place10']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb2['jawaban']; ?></center></td>
                    </tr>
                    <?php }}} ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header font-setting-3 bg-color-primary">
                <h3 class="card-title">Pertanyaan Price</h3>
                <div></div>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus font-setting-4"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Question</center></th>
                      <th><center>Score</center></th>
                      <th><center>Result</center></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                      $no = array(1,2,3,4,5,6,7,8,9,10);
                      while ($rowprice = mysqli_fetch_array($qryprice)) {
                        if ($rowprice['j_price1'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price1]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[0]; ?></center></td>
                      <td><?php if ($pprice[0] != '') {
                        echo $pprice[0];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price1']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price2'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price2]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[1]; ?></center></td>
                      <td><?php if ($pprice[1] != '') {
                        echo $pprice[1];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price2']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price3'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price3]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[2]; ?></center></td>
                      <td><?php if ($pprice[2] != '') {
                        echo $pprice[2];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price3']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price4'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price4]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[3]; ?></center></td>
                      <td><?php if ($pprice[3] != '') {
                        echo $pprice[3];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price4']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price5'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price5]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[4]; ?></center></td>
                      <td><?php if ($pprice[4] != '') {
                        echo $pprice[4];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price5']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price6'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price6]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[5]; ?></center></td>
                      <td><?php if ($pprice[5] != '') {
                        echo $pprice[5];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price6']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price7'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price7]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[6]; ?></center></td>
                      <td><?php if ($pprice[6] != '') {
                        echo $pprice[6];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price7']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price8'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price8]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[7]; ?></center></td>
                      <td><?php if ($pprice[7] != '') {
                        echo $pprice[7];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price8']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price9'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price9]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[8]; ?></center></td>
                      <td><?php if ($pprice[8] != '') {
                        echo $pprice[8];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price9']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowprice['j_price10'] != 0) {
                          $sqljwb3 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowprice[j_price10]'");
                          while ($rowjwb3 = mysqli_fetch_array($sqljwb3)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[9]; ?></center></td>
                      <td><?php if ($pprice[9] != '') {
                        echo $pprice[9];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowprice['j_price10']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb3['jawaban']; ?></center></td>
                    </tr>
                    <?php }}} ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header font-setting-3 bg-color-primary">
                <h3 class="card-title">Pertanyaan Produk</h3>
                <div></div>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus font-setting-4"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Question</center></th>
                      <th><center>Score</center></th>
                      <th><center>Result</center></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                      $no = array(1,2,3,4,5,6,7,8,9,10);
                      while ($rowproduk = mysqli_fetch_array($qryproduk)) {
                        if ($rowproduk['j_produk1'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk1]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[0]; ?></center></td>
                      <td><?php if ($pproduk[0] != '') {
                        echo $pproduk[0];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk1']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk2'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk2]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[1]; ?></center></td>
                      <td><?php if ($pproduk[1] != '') {
                        echo $pproduk[1];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk2']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk3'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk3]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[2]; ?></center></td>
                      <td><?php if ($pproduk[2] != '') {
                        echo $pproduk[2];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk3']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk4'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk4]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[3]; ?></center></td>
                      <td><?php if ($pproduk[3] != '') {
                        echo $pproduk[3];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk4']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk5'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk5]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[4]; ?></center></td>
                      <td><?php if ($pproduk[4] != '') {
                        echo $pproduk[4];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk5']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk6'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk6]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[5]; ?></center></td>
                      <td><?php if ($pproduk[5] != '') {
                        echo $pproduk[5];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk6']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk7'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk7]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[6]; ?></center></td>
                      <td><?php if ($pproduk[6] != '') {
                        echo $pproduk[6];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk7']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk8'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk8]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[7]; ?></center></td>
                      <td><?php if ($pproduk[7] != '') {
                        echo $pproduk[7];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk8']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk9'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk9]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[8]; ?></center></td>
                      <td><?php if ($pproduk[8] != '') {
                        echo $pproduk[8];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk9']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowproduk['j_produk10'] != 0) {
                          $sqljwb4 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowproduk[j_produk10]'");
                          while ($rowjwb4 = mysqli_fetch_array($sqljwb4)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[9]; ?></center></td>
                      <td><?php if ($pproduk[9] != '') {
                        echo $pproduk[9];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowproduk['j_produk10']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb4['jawaban']; ?></center></td>
                    </tr>
                    <?php }}} ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header font-setting-3 bg-color-primary">
                <h3 class="card-title">Pertanyaan Promotion</h3>
                <div></div>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus font-setting-4"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Question</center></th>
                      <th><center>Score</center></th>
                      <th><center>Result</center></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                      $no = array(1,2,3,4,5,6,7,8,9,10);
                      while ($rowpromotion = mysqli_fetch_array($qrypromotion)) {
                        if ($rowpromotion['j_promotion1'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion1]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[0]; ?></center></td>
                      <td><?php if ($ppromotion[0] != '') {
                        echo $ppromotion[0];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion1']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion2'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion2]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[1]; ?></center></td>
                      <td><?php if ($ppromotion[1] != '') {
                        echo $ppromotion[1];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion2']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion3'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion3]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[2]; ?></center></td>
                      <td><?php if ($ppromotion[2] != '') {
                        echo $ppromotion[2];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion3']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion4'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion4]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[3]; ?></center></td>
                      <td><?php if ($ppromotion[3] != '') {
                        echo $ppromotion[3];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion4']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion5'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion5]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[4]; ?></center></td>
                      <td><?php if ($ppromotion[4] != '') {
                        echo $ppromotion[4];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion5']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion6'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion6]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[5]; ?></center></td>
                      <td><?php if ($ppromotion[5] != '') {
                        echo $ppromotion[5];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion6']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion7'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion7]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[6]; ?></center></td>
                      <td><?php if ($ppromotion[6] != '') {
                        echo $ppromotion[6];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion7']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion8'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion8]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[7]; ?></center></td>
                      <td><?php if ($ppromotion[7] != '') {
                        echo $ppromotion[7];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion8']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion9'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion9]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[8]; ?></center></td>
                      <td><?php if ($ppromotion[8] != '') {
                        echo $ppromotion[8];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion9']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}
                        if ($rowpromotion['j_promotion10'] != 0) {
                          $sqljwb5 = mysqli_query($koneksi, "SELECT * FROM tb_jawaban WHERE id='$rowpromotion[j_promotion10]'");
                          while ($rowjwb5 = mysqli_fetch_array($sqljwb5)) {
                    ?>  
                    <tr>
                      <td width="6.5%"><center><?php echo $no[9]; ?></center></td>
                      <td><?php if ($ppromotion[9] != '') {
                        echo $ppromotion[9];
                      }else{
                        echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                      }?></td>
                      <td width="6.5%"><center><?php echo $rowpromotion['j_promotion10']; ?></center></td>
                      <td width="13%"><center><?php echo $rowjwb5['jawaban']; ?></center></td>
                    </tr>
                    <?php }}} ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>
                      <?php
                        $sql1 = "SELECT no_qb, time_steam FROM tb_kritik WHERE id='$id'";
                        $qry1 = mysqli_query ($koneksi, $sql1) or die ("query kritik salah");
                  
                        $sqlpromotion2 = "SELECT * FROM tb_promotion WHERE id='$id'";
                        $qrypromotion2 = mysqli_query($koneksi, $sqlpromotion2) or die ("query promotion salah");

                        $sqlpeople2 = "SELECT * FROM tb_people WHERE id='$id'";
                        $qrypeople2 = mysqli_query($koneksi, $sqlpeople2) or die ("query people salah");

                        $sqlproduk2 = "SELECT * FROM tb_produk WHERE id='$id'";
                        $qryproduk2 = mysqli_query($koneksi, $sqlproduk2) or die ("query produk salah");

                        $sqlprice2 = "SELECT * FROM tb_price WHERE id='$id'";
                        $qryprice2 = mysqli_query($koneksi, $sqlprice2) or die ("query price salah");

                        $sqlplace2 = "SELECT * FROM tb_place WHERE id='$id'";
                        $qryplace2 = mysqli_query($koneksi, $sqlplace2) or die ("query place salah");

                        // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                        while ($data1 = mysqli_fetch_array($qry1)) {
                        $datapromotion = mysqli_fetch_array($qrypromotion2);
                        $datapeople = mysqli_fetch_array($qrypeople2);
                        $dataproduk = mysqli_fetch_array($qryproduk2);
                        $dataprice = mysqli_fetch_array($qryprice2);
                        $dataplace = mysqli_fetch_array($qryplace2);
                        $noqb = $data1['no_qb'];
                        $promotion1 = $datapromotion['j_promotion1'];
                        $promotion2 = $datapromotion['j_promotion2'];
                        $promotion3 = $datapromotion['j_promotion3'];
                        $promotion4 = $datapromotion['j_promotion4'];
                        $promotion5 = $datapromotion['j_promotion5'];
                        $promotion6 = $datapromotion['j_promotion6'];
                        $promotion7 = $datapromotion['j_promotion7'];
                        $promotion8 = $datapromotion['j_promotion8'];
                        $promotion9 = $datapromotion['j_promotion9'];
                        $promotion10 = $datapromotion['j_promotion10'];
                        $people1 = $datapeople['j_people1'];
                        $people2 = $datapeople['j_people2'];
                        $people3 = $datapeople['j_people3'];
                        $people4 = $datapeople['j_people4'];
                        $people5 = $datapeople['j_people5'];
                        $people6 = $datapeople['j_people6'];
                        $people7 = $datapeople['j_people7'];
                        $people8 = $datapeople['j_people8'];
                        $people9 = $datapeople['j_people9'];
                        $people10 = $datapeople['j_people10'];
                        $produk1 = $dataproduk['j_produk1'];
                        $produk2 = $dataproduk['j_produk2'];
                        $produk3 = $dataproduk['j_produk3'];
                        $produk4 = $dataproduk['j_produk4'];
                        $produk5 = $dataproduk['j_produk5'];
                        $produk6 = $dataproduk['j_produk6'];
                        $produk7 = $dataproduk['j_produk7'];
                        $produk8 = $dataproduk['j_produk8'];
                        $produk9 = $dataproduk['j_produk9'];
                        $produk10 = $dataproduk['j_produk10'];
                        $price1 = $dataprice['j_price1'];
                        $price2 = $dataprice['j_price2'];
                        $price3 = $dataprice['j_price3'];
                        $price4 = $dataprice['j_price4'];
                        $price5 = $dataprice['j_price5'];
                        $price6 = $dataprice['j_price6'];
                        $price7 = $dataprice['j_price7'];
                        $price8 = $dataprice['j_price8'];
                        $price9 = $dataprice['j_price9'];
                        $price10 = $dataprice['j_price10'];
                        $place1 = $dataplace['j_place1'];
                        $place2 = $dataplace['j_place2'];
                        $place3 = $dataplace['j_place3'];
                        $place4 = $dataplace['j_place4'];
                        $place5 = $dataplace['j_place5'];
                        $place6 = $dataplace['j_place6'];
                        $place7 = $dataplace['j_place7'];
                        $place8 = $dataplace['j_place8'];
                        $place9 = $dataplace['j_place9'];
                        $place10 = $dataplace['j_place10'];

                        if ($promotion1 != 0) {
                          $npromotion1 = 1;
                        }

                        if ($promotion2 != 0) {
                          $npromotion2 = 1;
                        }

                        if ($promotion3 != 0) {
                          $npromotion3 = 1;
                        }

                        if ($promotion4 != 0) {
                          $npromotion4 = 1;
                        }

                        if ($promotion5 != 0) {
                          $npromotion5 = 1;
                        }

                        if ($promotion6 != 0) {
                          $npromotion6 = 1;
                        }

                        if ($promotion7 != 0) {
                          $npromotion7 = 1;
                        }

                        if ($promotion8 != 0) {
                          $npromotion8 = 1;
                        }

                        if ($promotion9 != 0) {
                          $npromotion9 = 1;
                        }

                        if ($promotion10 != 0) {
                          $npromotion10 = 1;
                        }

                        if ($people1 != 0) {
                          $npeople1 = 1;
                        }

                        if ($people2 != 0) {
                          $npeople2 = 1;
                        }

                        if ($people3 != 0) {
                          $npeople3 = 1;
                        }

                        if ($people4 != 0) {
                          $npeople4 = 1;
                        }

                        if ($people5 != 0) {
                          $npeople5 = 1;
                        }

                        if ($people6 != 0) {
                          $npeople6 = 1;
                        }

                        if ($people7 != 0) {
                          $npeople7 = 1;
                        }

                        if ($people8 != 0) {
                          $npeople8 = 1;
                        }

                        if ($people9 != 0) {
                          $npeople9 = 1;
                        }

                        if ($people10 != 0) {
                          $npeople10 = 1;
                        }

                        if ($produk1 != 0) {
                          $nproduk1 = 1;
                        }

                        if ($produk2 != 0) {
                          $nproduk2 = 1;
                        }

                        if ($produk3 != 0) {
                          $nproduk3 = 1;
                        }

                        if ($produk4 != 0) {
                          $nproduk4 = 1;
                        }

                        if ($produk5 != 0) {
                          $nproduk5 = 1;
                        }

                        if ($produk6 != 0) {
                          $nproduk6 = 1;
                        }

                        if ($produk7 != 0) {
                          $nproduk7 = 1;
                        }

                        if ($produk8 != 0) {
                          $nproduk8 = 1;
                        }

                        if ($produk9 != 0) {
                          $nproduk9 = 1;
                        }

                        if ($produk10 != 0) {
                          $nproduk10 = 1;
                        }

                        if ($price1 != 0) {
                          $nprice1 = 1;
                        }

                        if ($price2 != 0) {
                          $nprice2 = 1;
                        }

                        if ($price3 != 0) {
                          $nprice3 = 1;
                        }

                        if ($price4 != 0) {
                          $nprice4 = 1;
                        }

                        if ($price5 != 0) {
                          $nprice5 = 1;
                        }

                        if ($price6 != 0) {
                          $nprice6 = 1;
                        }

                        if ($price7 != 0) {
                          $nprice7 = 1;
                        }

                        if ($price8 != 0) {
                          $nprice8 = 1;
                        }

                        if ($price9 != 0) {
                          $nprice9 = 1;
                        }

                        if ($price10 != 0) {
                          $nprice10 = 1;
                        }

                        if ($place1 != 0) {
                          $nplace1 = 1;
                        }

                        if ($place2 != 0) {
                          $nplace2 = 1;
                        }

                        if ($place3 != 0) {
                          $nplace3 = 1;
                        }

                        if ($place4 != 0) {
                          $nplace4 = 1;
                        }

                        if ($place5 != 0) {
                          $nplace5 = 1;
                        }

                        if ($place6 != 0) {
                          $nplace6 = 1;
                        }

                        if ($place7 != 0) {
                          $nplace7 = 1;
                        }

                        if ($place8 != 0) {
                          $nplace8 = 1;
                        }

                        if ($place9 != 0) {
                          $nplace9 = 1;
                        }

                        if ($place10 != 0) {
                          $nplace10 = 1;
                        }

                        $sum =$promotion1 + $promotion2 + $promotion3 + $promotion4 + $promotion5 + $promotion6 + $promotion7 + $promotion8 + $promotion9 + $promotion10 + $people1 + $people2 + $people3 + $people4 + $people5 + $people6 + $people7 + $people8 + $people9 + $people10 + $produk1 + $produk2 + $produk3 + $produk4 + $produk5 + $produk6 + $produk7 + $produk8 + $produk9 + $produk10 + $price1 + $price2 + $price3 + $price4 + $price5 + $price6 + $price7 + $price8 + $price9 + $price10 + $place1 + $place2 + $place3 + $place4 + $place5 + $place6 + $place7 + $place8 + $place9 + $place10;

                        $nilaipembagi = $npromotion1 + $npromotion2 + $npromotion3 + $npromotion4 + $npromotion5 + $npromotion6 + $npromotion7 + $npromotion8 + $npromotion9 + $npromotion10 + $npeople1 + $npeople2 + $npeople3 + $npeople4 + $npeople5 + $npeople6 + $npeople7 + $npeople8 + $npeople9 + $npeople10 + $nproduk1 + $nproduk2 + $nproduk3 + $nproduk4 + $nproduk5 + $nproduk6 + $nproduk7 + $nproduk8 + $nproduk9 + $nproduk10 + $nprice1 + $nprice2 + $nprice3 + $nprice4 + $nprice5 + $nprice6 + $nprice7 + $nprice8 + $nprice9 + $nprice10 + $nplace + $nplace1 + $nplace2 + $nplace3 + $nplace4 + $nplace5 + $nplace6 + $nplace7 + $nplace8 + $nplace9 + $nplace10;
                    
                        $hasil = $sum / $nilaipembagi;
                        $hasilkonversi = number_format($hasil,1,",",".");

                        $hasilpersen = $hasil / 5 * 100;
                        $hasilpersenkonversi = number_format($hasilpersen,1,",",".");

  
                    ?>
                    <tr>
                      <th>No QB</th>
                      <th width="12%"><center><?php echo $noqb; ?></center></th>
                    </tr>
                    <tr>
                      <th>Total Presentation</th>
                      <th width="12%"><center><?php echo $hasilpersenkonversi;?>%</center></th>
                    </tr>
                    <tr>
                      <th>Total Score</th>
                      <th width="12%"><center><?php echo $hasilkonversi; ?></center></th>
                    </tr>
                    <?php }?>
                    </thead>
                  </table>
                </div>
                <!-- /.table-responsive -->
                <div class="card-footer clearfix">
                  <a href="allscore.php" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp;</a>
                  <a href="printdetail.php?id=<?php echo $id; ?>" target="blank" class="btn btn-primary float-right"><i class="nav-icon fas fa-print"></i> Print</a>
                </div>
                <!-- /.card-footer -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
          




  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <!-- <a href="http://adminlte.io">AdminLTE.io</a> -->.</strong>
    All rights reserved.
  </footer>
</div>


<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
