<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
</head>

<?php
	include 'koneksi.php';
?>

<body>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#">
		<img src="gambar/logomitra10.svg" width="100" height="45">
	</a>

  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  	</button>

  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
    	<ul class="navbar-nav mr-auto">
    		<li><p><h4>KUESIONER TINGKAT KEPUASAN PELANGGAN</h4></p></li>
    	</ul>
    	<a href="login.php" class="btn btn-outline-primary mr-2">
    		<i class="fa fa-sign-in-alt"></i> Login
    	</a>
  	</div>

</nav>
<div class="hidden">Hidden</div>
<div class="tengah kotak_tengah">
	<div class="kotak_dalam">
		<p><h5>Terimakasih telah berbelanja di Mitra10 Q-Big <i class="far fa-smile"></i></h5></p>
	</div>
		Silahkan masukkan No QB anda yang berada di struk pembelanjaan </br>
		<i style="padding-left: 67px" class="fas fa-arrow-down"></i> <i class="fas fa-arrow-down"></i> <i class="fas fa-arrow-down"></i></br>
		QB : <input required minlength="" maxlength="" class="textbox1" type="text" name="" value="" >
		</br>
		</br>


		<table class="tabel-index">
			<tr>
				<th style="padding: 0px 10px;">No.</th>
				<th><strong>Pertanyaan Produk</strong></th>
			</tr>
				<?php
				$no1 = 1;
				$strSQL1 = "SELECT * FROM tb_pertanyaan_produk WHERE id";
				$query1 = mysqli_query ($koneksi, $strSQL1) or die ("Query Salah");
				while ($row1 = mysqli_fetch_array($query1)){
				$id1 = $row1 ['id'];

				if ($row1["p_produk"]) {
					echo "<tr>";
					echo "<td align='center' valign='top'>".$no1++."</td>";
					echo "<td>".$row1["p_produk"]."</br></br>";
					echo "<input type='radio' name='j_produk1' value='5'> Sangat Setuju &emsp; ";
					echo "<input type='radio' name='j_produk1' value='4'> Setuju &emsp;" ;
					echo "<input type='radio' name='j_produk1' value='3'> Netral &emsp; ";
					echo "<input type='radio' name='j_produk1' value='2'> Tidak Setuju &emsp; ";
					echo "<input type='radio' name='j_produk1' value='1'> Sangat Tidak Setuju &emsp; </br></br></td>";
				echo "</tr>";
				}else{
					echo "<td>Silahkan membuat pertanyaan tentang Produk</td>";
				echo "</tr>";
				}}
				?>
		</table>
		</br>
		</br>
		</br>

  		<table class="tabel-index">
			<tr>
				<th style="padding: 0px 10px;">No.</th>
				<th><strong>Pertanyaan Place</strong></th>
			</tr>
			<tr>
				<?php
				$no2 = 1;
				$strSQL2 = "SELECT * FROM tb_pertanyaan_place WHERE id";
				$query2 = mysqli_query ($koneksi, $strSQL2) or die ("Query Salah");
				while ($row2 = mysqli_fetch_array($query2)){
				$id2 = $row2 ['id'];

				if ($row2["p_place"]) {
					echo "<td align='center' valign='top'>".$no2++."</td>";
					echo "<td>".$row2["p_place"]."</br></br>";
					echo "<input type='radio' name='j_place1' value='5'> Sangat Setuju &emsp; ";
					echo "<input type='radio' name='j_place1' value='4'> Setuju &emsp;" ;
					echo "<input type='radio' name='j_place1' value='3'> Netral &emsp; ";
					echo "<input type='radio' name='j_place1' value='2'> Tidak Setuju &emsp; ";
					echo "<input type='radio' name='j_place1' value='1'> Sangat Tidak Setuju &emsp; </br></br></td>";
				echo "</tr>";
				}else{
					echo "<td>Silahkan membuat pertanyaan tentang Produk</td>";
				echo "</tr>";
				}}
				?>
			</tr>
		</table>
		</br>
		</br>
		</br>


		<table class="tabel-index">
			<tr>
				<th style="padding: 0px 10px;">No.</th>
				<th><strong>Pertanyaan Price</strong></th>
			</tr>
			<tr>
				<?php
				$no3 = 1;
				$strSQL3 = "SELECT * FROM tb_pertanyaan_price WHERE id";
				$query3 = mysqli_query ($koneksi, $strSQL3) or die ("Query Salah");
				while ($row3 = mysqli_fetch_array($query3)){
				$id3 = $row3 ['id'];

				if ($row3["p_price"]) {
					echo "<td align='center' valign='top'>".$no3++."</td>";
					echo "<td align='left'>".$row3["p_price"]."</br></br>";
					echo "<input type='radio' name='j_price1' value='5'> Sangat Setuju &emsp; ";
					echo "<input type='radio' name='j_price1' value='4'> Setuju &emsp;" ;
					echo "<input type='radio' name='j_price1' value='3'> Netral &emsp; ";
					echo "<input type='radio' name='j_price1' value='2'> Tidak Setuju &emsp; ";
					echo "<input type='radio' name='j_price1' value='1'> Sangat Tidak Setuju &emsp; </br></br></td>";
				echo "</tr>";
				}else{
					echo "<td>Silahkan membuat pertanyaan tentang Produk</td>";
				echo "</tr>";
				}}
				?>
			</tr>
		</table>
		</br>
		</br>
		</br>

		<table class="tabel-index">
			<tr>
				<th style="padding: 0px 10px;">No.</th>
				<th><strong>Pertanyaan People</strong></th>
			</tr>
			<tr>
				<?php
				$no4 = 1;
				$strSQL4 = "SELECT * FROM tb_pertanyaan_people WHERE id";
				$query4 = mysqli_query ($koneksi, $strSQL4) or die ("Query Salah");
				while ($row4 = mysqli_fetch_array($query4)){
				$id4 = $row4 ['id'];

				if ($row4["p_people"]) {
					echo "<td align='center' valign='top'>".$no4++."</td>";
					echo "<td>".$row4["p_people"]."</br></br>";
					echo "<input type='radio' name='j_people1' value='5'> Sangat Setuju &emsp; ";
					echo "<input type='radio' name='j_people1' value='4'> Setuju &emsp;" ;
					echo "<input type='radio' name='j_people1' value='3'> Netral &emsp; ";
					echo "<input type='radio' name='j_people1' value='2'> Tidak Setuju &emsp; ";
					echo "<input type='radio' name='j_people1' value='1'> Sangat Tidak Setuju &emsp; </br></br></td>";
				echo "</tr>";
				}else{
					echo "<td>Silahkan membuat pertanyaan tentang Produk</td>";
				echo "</tr>";
				}}
				?>
			</tr>
		</table>
		</br>
		</br>
		</br>

		<table class="tabel-index">
			<tr>
				<th style="padding: 0px 10px;">No.</th>
				<th><strong>Pertanyaan Promotion</strong></th>
			</tr>
			<tr>
				<?php
				$no5 = 1;
				$strSQL5 = "SELECT * FROM tb_pertanyaan_promotion WHERE id";
				$query5 = mysqli_query ($koneksi, $strSQL5) or die ("Query Salah");
				while ($row5 = mysqli_fetch_array($query5)){
				$id5 = $row5 ['id'];
				
				if ($row5["p_promotion"]) {
					echo "<td align='center' valign='top'>".$no5++."</td>";
					echo "<td>".$row5["p_promotion"]."</br></br>";
					echo "<input type='radio' name='j_promotion1' value='5'> Sangat Setuju &emsp; ";
					echo "<input type='radio' name='j_promotion1' value='4'> Setuju &emsp;" ;
					echo "<input type='radio' name='j_promotion1' value='3'> Netral &emsp; ";
					echo "<input type='radio' name='j_promotion1' value='2'> Tidak Setuju &emsp; ";
					echo "<input type='radio' name='j_promotion1' value='1'> Sangat Tidak Setuju &emsp; </br></br></td>";
				echo "</tr>";
				}else{
					echo "<td>Silahkan membuat pertanyaan tentang Produk</td>";
				echo "</tr>";
				}
				}?>
			</tr>
		</table>
		</br>
		</br>

			<span style="color: #6c6c6c">Kritik & Saran!</span>
			<textarea required minlength="" maxlength="" class="textbox2" type="text" name="" value="" ></textarea>

		</br>
		</br>

		<table width="100%">
			<tr>
				<td style="border: 0; padding: 0px;">
					<a href="#" class="btn btn-primary button-right">
					<i class="fas fa-check"></i> Selesai</a>
				</td>
			</tr>
		</table>

</div>


	<script src="assets/js/jquery.js"></script> 
	<script src="assets/js/popper.js"></script> 
	<script src="assets/js/bootstrap.js"></script>
</body>
</html>