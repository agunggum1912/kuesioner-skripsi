<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


<?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }

  $id = $_GET['id'];
  $sql = "SELECT id_level, level, username, nama, no_hp, tgl_lahir, jenis_kelamin FROM tb_user WHERE id='$id'";
  $hasil = mysqli_query ($koneksi, $sql);
  $data = mysqli_fetch_array($hasil);
  {
    $username = $data ['username'];
    $nama = $data ['nama'];
    $no_hp = $data ['no_hp'];
    $tgl_lahir = $data ['tgl_lahir'];
    $tgl_lahir3 = date("d-m-Y", strtotime($tgl_lahir));
    $jenis_kelamin = $data ['jenis_kelamin'];
    $level = $data ['level'];
  }

  if (isset($_POST['update'])) {
    $id2 = trim($_POST['id']);
    $nama2 = trim($_POST['nama']);
    $no_hp2 = trim($_POST['no_hp']);
    $tgl_lahir2 = $_POST['tgl_lahir'];
    $tgl_lahir4 = date("Y-m-d", strtotime($tgl_lahir2));
    $jenis_kelamin2 = trim($_POST['jenis_kelamin']);
    $level2 = $_POST['level'];

    if ($nama == $nama2) {
      if ($no_hp == $no_hp2) {
        if ($tgl_lahir3 == $tgl_lahir2) {
          if ($jenis_kelamin == $jenis_kelamin2) {
            if ($level == $level2) {
              echo "<script>alert('Tidak ada data yang berubah!');window.location='edituserid.php'; </script>";
            }
          }
        }
      }
    }

    if (preg_match("/Relationship Manager/", $level2) || preg_match("/Store Manager/", $level2)) {
    echo $hasilposition = "1";
    }else{
      echo $hasilposition = "2";
    }

    if (empty($nama2)) {
      echo "<script>alert('Name harus di isi!');history.go(-1)</script>";
    }elseif (empty($no_hp2)) {
      echo "<script>alert('Number Phone harus di isi!');history.go(-1)</script>";
    }elseif (empty($tgl_lahir2)) {
      echo "<script>alert('Date of Birth harus di isi!');history.go(-1)</script>";
    }elseif (empty($jenis_kelamin2)) {
      echo "<script>alert('Gender harus di isi!');history.go(-1)</script>";
    }elseif (empty($level2)) {
      echo "<script>alert('Position harus di isi!');history.go(-1)</script>";
    }elseif (strlen($nama2) >= 32) {
      echo "<script>alert('Panjang Name tidak boleh 32 Character!');history.go(-1)</script>";
    }elseif (strlen($no_hp2) <= 9 || strlen($no_hp2) >= 14) {
      echo "<script>alert('Number Phone hanya boleh 10-13 Angka!');history.go(-1)</script>";
    }elseif ($jenis_kelamin2 != "Female" && $jenis_kelamin2 != "Male") {
      echo "<script>alert('Gender Salah!');history.go(-1)</script>";
    }elseif ($level2 != "Administration" && $level2 != "Head of Division" && $level2 != "Human Resource Departement" && $level2 != "Manager On Duty" && $level2 != "Relationship Manager" && $level2 != "Shared Service Center" && $level2 != "Store Manager") {
      echo "<script>alert('Position Salah!');history.go(-1)</script>";
    }else{
      $sql2 = "UPDATE tb_user SET nama='$nama2', no_hp='$no_hp2', tgl_lahir='$tgl_lahir4', jenis_kelamin='$jenis_kelamin2', level='$level2', id_level='$hasilposition' WHERE id='$id2'";
      $hasil2 = mysqli_query ($koneksi, $sql2) or die ("query update salah");

      echo "<script>alert('Data telah terupdate.');window.location='edituserid.php'; </script>";
    }
  }

  
?>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <?php
            $strSQL = "SELECT tb_user.id, tb_user.username, tb_user.nama, tb_user.foto FROM tb_user WHERE username='$_SESSION[userlogin]' ";
            $query = mysqli_query ($koneksi, $strSQL) or die ("query salah");
            while ($row = mysqli_fetch_array($query)){
            $id = $row ['id'];

            echo $row["username"];
          ?> 
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="reportresult.php" class="brand-link">
      <img src="gambar/logom10.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Mitra10 Q-Big</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row ['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="setting.php" class="d-block">
            <?php echo $row["nama"]; }?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="reportresult.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Report Result
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview  menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deleteuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="edituserid.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Manage Question
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="addquestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deletequestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="editquestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit Question</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="lockscreen.php?username=<?php echo $_SESSION['userlogin']; ?>" class="nav-link">
              <font style="color: #ed1c24;"><i class="nav-icon fas fa-user-lock"></i></font>
              <p>
                LockScreen
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit User Id Proses</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="reportresult.php">Home</a></li>
              <li class="breadcrumb-item"><a href="edituserid.php">Manage User Id</a></li>
              <li class="breadcrumb-item active">Edit User Id</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="offset-lg-1 col-md-10">
            <!-- general form elements disabled -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit User Id</h3>
              </div>
              <form action="" method="post">
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Username</label>
                        <input name="username" type="text" class="form-control" value="<?php echo $username; ?>" disabled>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Name</label>
                        <input name="nama" type="text" class="form-control" value="<?php echo $nama; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Number Phone</label>
                          <div class="input-group">
                            <input name="no_hp" type="text" class="form-control" value="<?php echo $no_hp; ?>">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                          </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Date of Birth</label>
                          <div class="input-group date" id="reservationdate" data-target-input="nearest">
                            <input required name="tgl_lahir" type="text" class="form-control datetimepicker-input" data-target="#reservationdate" data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy" data-mask value="<?php echo $tgl_lahir3; ?>">
                              <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Gender</label>
                        <select name="jenis_kelamin" class="form-control">
                          <option value="<?php echo $jenis_kelamin; ?>"><?php echo $jenis_kelamin; ?></option>
                          <option value="Female">Female</option>
                          <option value="Male">Male</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Postion</label>
                        <select name="level" class="form-control">
                          <option value="<?php echo $level; ?>"><?php echo $level; ?></option>
                          <option value="Administration">Administration</option>
                          <option value="Head of Division">Head of Division</option>
                          <option value="Human Resource Departement">Human Resource Departement</option>
                          <option value="Manager On Duty">Manager On Duty</option>
                          <option value="Relationship Manager">Relationship Manager</option>
                          <option value="Shared Service Center">Shared Service Center</option>
                          <option value="Store Manager">Store Manager</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                <!-- /.card-body -->
                <div class="card-footer">
                  <button onclick="return confirm('Apakah anda yakin ingin mengubah data username <?php echo $username ;?>?')" type="submit" name="update" class="btn btn-primary float-right">Submit</button>
                  <a href="edituserid.php" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp; </a>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">
            
            
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->





  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <!-- <a href="http://adminlte.io">AdminLTE.io</a> -->.</strong>
    All rights reserved.
  </footer>
</div>


<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#imageResult')
          .attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
      readURL(input);
    });
  });

  var input = document.getElementById( 'upload' );
  var infoArea = document.getElementById( 'upload-label' );

  input.addEventListener( 'change', showFileName );
  function showFileName( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea.textContent = 'File name: ' + fileName;
  }
</script>
</body>
</html>
