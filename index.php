<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style-index.css">
  <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">

  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
</head>

<?php
  include 'koneksi.php';

  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
  if (isset($_POST['tambah'])) {
    $t_produk1 = $_POST['j_produk1'];
    $t_produk2 = $_POST['j_produk2'];
    $t_produk3 = $_POST['j_produk3'];
    $t_produk4 = $_POST['j_produk4'];
    $t_produk5 = $_POST['j_produk5'];
    $t_produk6 = $_POST['j_produk6'];
    $t_produk7 = $_POST['j_produk7'];
    $t_produk8 = $_POST['j_produk8'];
    $t_produk9 = $_POST['j_produk9'];
    $t_produk10 = $_POST['j_produk10'];
    $t_people1 = $_POST['j_people1'];
    $t_people2 = $_POST['j_people2'];
    $t_people3 = $_POST['j_people3'];
    $t_people4 = $_POST['j_people4'];
    $t_people5 = $_POST['j_people5'];
    $t_people6 = $_POST['j_people6'];
    $t_people7 = $_POST['j_people7'];
    $t_people8 = $_POST['j_people8'];
    $t_people9 = $_POST['j_people9'];
    $t_people10 = $_POST['j_people10'];
    $t_place1 = $_POST['j_place1'];
    $t_place2 = $_POST['j_place2'];
    $t_place3 = $_POST['j_place3'];
    $t_place4 = $_POST['j_place4'];
    $t_place5 = $_POST['j_place5'];
    $t_place6 = $_POST['j_place6'];
    $t_place7 = $_POST['j_place7'];
    $t_place8 = $_POST['j_place8'];
    $t_place9 = $_POST['j_place9'];
    $t_place10 = $_POST['j_place10'];
    $t_price1 = $_POST['j_price1'];
    $t_price2 = $_POST['j_price2'];
    $t_price3 = $_POST['j_price3'];
    $t_price4 = $_POST['j_price4'];
    $t_price5 = $_POST['j_price5'];
    $t_price6 = $_POST['j_price6'];
    $t_price7 = $_POST['j_price7'];
    $t_price8 = $_POST['j_price8'];
    $t_price9 = $_POST['j_price9'];
    $t_price10 = $_POST['j_price10'];
    $t_promotion1 = $_POST['j_promotion1'];
    $t_promotion2 = $_POST['j_promotion2'];
    $t_promotion3 = $_POST['j_promotion3'];
    $t_promotion4 = $_POST['j_promotion4'];
    $t_promotion5 = $_POST['j_promotion5'];
    $t_promotion6 = $_POST['j_promotion6'];
    $t_promotion7 = $_POST['j_promotion7'];
    $t_promotion8 = $_POST['j_promotion8'];
    $t_promotion9 = $_POST['j_promotion9'];
    $t_promotion10 = $_POST['j_promotion10'];
    $kritik = $_POST['kritik'];
    $noqb = $_POST['noqb'];

    $sqlcek = "SELECT no_qb FROM tb_kritik";
    $querycek = mysqli_query($koneksi, $sqlcek) or die ("Query Cek Salah");
    while ($data = mysqli_fetch_array($querycek)){
    $ceknoqb = $data['no_qb'];

      if (empty($noqb)) {
        echo "<script>alert('Silahkan masukkan No QB yang tertera di Struk pembelanjaan anda!');history.go(-1)</script>";
      }elseif ($noqb == $ceknoqb) {
        echo "<script>alert('Ups! No QB yang anda masukkan salah.');history.go(-1)</script>";
      }elseif (strlen($noqb) > 10 || strlen($noqb) < 5) {
        echo "<script>alert('No QB hanya 10 digit!');history.go(-1)</script>";
      }elseif (strlen($kritik) > 300) {
        echo "<script>alert('Kritik & Saran maximal 300 panjang karakter!');history.go(-1)</script>";
      }else{
            echo "<script>alert('Terimakasih survey anda sangat berharga bagi kami:).') ;window.location='index.php'; </script>";
      }
    }


    $sql = "INSERT INTO tb_kritik(id,kritik,no_qb,time_steam) VALUES (NULL,'$kritik','$noqb',current_timestamp())";
    $hasil = mysqli_query($koneksi, $sql);

    $strSQL = "SELECT id, kritik FROM tb_kritik WHERE no_qb='$noqb'";
    $query = mysqli_query ($koneksi, $strSQL) or die ("Query Kritik salah");
    while ($row = mysqli_fetch_array($query)){
      $id = $row['id'];

      $sql1 = "UPDATE tb_place SET j_place1='$t_place1', j_place2='$t_place2', j_place3='$t_place3', j_place4='$t_place4', j_place5='$t_place5', j_place6='$t_place6', j_place7='$t_place7', j_place8='$t_place8', j_place9='$t_place9', j_place10='$t_place10' WHERE id='$id'";
      $hasil1 = mysqli_query($koneksi, $sql1);

      $sql2 = "UPDATE tb_price SET j_price1='$t_price1', j_price2='$t_price2', j_price3='$t_price3', j_price4='$t_price4', j_price5='$t_price5', j_price6='$t_price6', j_price7='$t_price7', j_price8='$t_price8', j_price9='$t_price9', j_price10='$t_price10' WHERE id='$id'";
      $hasil2 = mysqli_query($koneksi, $sql2);

      $sql3 = "UPDATE tb_produk SET j_produk1='$t_produk1', j_produk2='$t_produk2', j_produk3='$t_produk3', j_produk4='$t_produk4', j_produk5='$t_produk5', j_produk6='$t_produk6', j_produk7='$t_produk7', j_produk8='$t_produk8', j_produk9='$t_produk9', j_produk10='$t_produk10' WHERE id='$id'";
      $hasil3 = mysqli_query($koneksi, $sql3);
    
      $sql4 = "UPDATE tb_promotion SET j_promotion1='$t_promotion1', j_promotion2='$t_promotion2', j_promotion3='$t_promotion3', j_promotion4='$t_promotion4', j_promotion5='$t_promotion5', j_promotion6='$t_promotion6', j_promotion7='$t_promotion7', j_promotion8='$t_promotion8', j_promotion9='$t_promotion9', j_promotion10='$t_promotion10' WHERE id='$id'";
      $hasil4 = mysqli_query($koneksi, $sql4);

      $sql5 = "UPDATE tb_people SET j_people1='$t_people1', j_people2='$t_people2', j_people3='$t_people3', j_people4='$t_people4', j_people5='$t_people5', j_people6='$t_people6', j_people7='$t_people7', j_people8='$t_people8', j_people9='$t_people9', j_people10='$t_people10' WHERE id='$id'";
      $hasil5 = mysqli_query($koneksi, $sql5);
    }

    
  }

  
?>

<body>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <img src="gambar/logomitra10.svg" width="100" height="45">
  </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li><p><h4>KUESIONER TINGKAT KEPUASAN PELANGGAN</h4></p></li>
      </ul>
      <a href="login.php" class="btn btn-outline-primary mr-2">
        <i class="fa fa-sign-in-alt"></i> Login
      </a>
    </div>

</nav>
<div class="hidden">Hidden</div>


  <form name="jawaban_proses" method="post" action="">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="offset-md-2 col-md-8">
            <div class="card-index">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="card-header2">
                  <p><h5>Terimakasih telah berbelanja di Mitra10 Q-Big <i class="far fa-smile"></i></h5></p>
                </div>
                Silahkan masukkan No Transaksi anda yang berada di atas struk pembelanjaan</br>
                <i style="padding-left: 135px" class="fas fa-arrow-down"></i> <i class="fas fa-arrow-down"></i> <i class="fas fa-arrow-down"></i></br>
                No Transaksi : <input required minlength="6" maxlength="10" class="textbox1" type="text" name="noqb">
                </br>
                </br>
                <table class="table table-index">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Pertanyaan Produk</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no1 = 1;
                      $noj1 = 0;
                      $strSQL1 = "SELECT * FROM tb_pertanyaan_produk WHERE id";
                      $query1 = mysqli_query ($koneksi, $strSQL1) or die ("Query Salah");
                      while ($row1 = mysqli_fetch_array($query1)){
                      $id1 = $row1 ['id'];
                      $noj1++;  

                      if ($row1["p_produk"]) { ?>
                        <tr>
                          <td align='center' valign='top'><?php echo $no1++; ?></td>
                          <td><?php echo $row1['p_produk']; ?></br></br>
                            <input required type='radio' name='<?php echo "j_produk"."$noj1";?>' value='5'> Setuju Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_produk"."$noj1";?>' value='4'> Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_produk"."$noj1";?>' value='3'> Netral &emsp;
                            <input required type='radio' name='<?php echo "j_produk"."$noj1";?>' value='2'> Tidak Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_produk"."$noj1";?>' value='1'> Sangat Tidak Setuju &emsp; </br></br>
                          </td>
                        </tr>
                      <?php }else{ ?>
                        <tr>
                          <td>Silahkan membuat pertanyaan tentang Produk</td>
                        </tr>
                      <?php }} ?>
                  </tbody>
                </table>
                </br>
                </br>
                </br>

                <table class="table table-index">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Pertanyaan People</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no2 = 1;
                      $noj2 = 0;
                      $strSQL2 = "SELECT * FROM tb_pertanyaan_people WHERE id";
                      $query2 = mysqli_query ($koneksi, $strSQL2) or die ("Query Salah");
                      while ($row2 = mysqli_fetch_array($query2)){
                      $id2 = $row2 ['id'];
                      $noj2++;

                      if ($row2["p_people"]) { ?>
                        <tr>
                          <td align='center' valign='top'><?php echo $no2++; ?></td>
                          <td><?php echo $row2["p_people"]; ?></br></br>
                            <input required type='radio' name='<?php echo "j_people"."$noj2";?>' value='5'> Sangat Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_people"."$noj2";?>' value='4'> Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_people"."$noj2";?>' value='3'> Netral &emsp;
                            <input required type='radio' name='<?php echo "j_people"."$noj2";?>' value='2'> Tidak Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_people"."$noj2";?>' value='1'> Sangat Tidak Setuju &emsp; </br></br>
                          </td>
                        </tr>
                      <?php }else{ ?>
                        <tr>
                          <td>Silahkan membuat pertanyaan tentang People</td>
                        </tr>
                      <?php }} ?>
                  </tbody>
                </table>
                </br>
                </br>
                </br>

                <table class="table table-index">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Pertanyaan Place</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no3 = 1;
                      $noj3 = 0;
                      $strSQL3 = "SELECT * FROM tb_pertanyaan_place WHERE id";
                      $query3 = mysqli_query ($koneksi, $strSQL3) or die ("Query Salah");
                      while ($row3 = mysqli_fetch_array($query3)){
                      $id3 = $row3 ['id'];
                      $noj3++;

                      if ($row3["p_place"]) { ?>
                        <tr>
                          <td align='center' valign='top'><?php echo $no3++; ?></td>
                          <td><?php echo $row3["p_place"]; ?></br></br>
                            <input required type='radio' name='<?php echo "j_place"."$noj3";?>' value='5'> Sangat Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_place"."$noj3";?>' value='4'> Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_place"."$noj3";?>' value='3'> Netral &emsp;
                            <input required type='radio' name='<?php echo "j_place"."$noj3";?>' value='2'> Tidak Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_place"."$noj3";?>' value='1'> Sangat Tidak Setuju &emsp; </br></br>
                          </td>
                        </tr>
                      <?php }else{ ?>
                        <tr>
                          <td>Silahkan membuat pertanyaan tentang Place</td>
                        </tr>
                      <?php }} ?>
                  </tbody>
                </table>
                </br>
                </br>
                </br>

                <table class="table table-index">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Pertanyaan Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no4 = 1;
                      $noj4 = 0;
                      $strSQL4 = "SELECT * FROM tb_pertanyaan_price WHERE id";
                      $query4 = mysqli_query ($koneksi, $strSQL4) or die ("Query Salah");
                      while ($row4 = mysqli_fetch_array($query4)){
                      $id4 = $row4 ['id'];
                      $noj4++;

                      if ($row4["p_price"]) { ?>
                        <tr>
                          <td align='center' valign='top'><?php echo $no4++; ?></td>
                          <td><?php echo $row4["p_price"]; ?></br></br>
                            <input required type='radio' name='<?php echo "j_price"."$noj4";?>' value='5'> Sangat Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_price"."$noj4";?>' value='4'> Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_price"."$noj4";?>' value='3'> Netral &emsp;
                            <input required type='radio' name='<?php echo "j_price"."$noj4";?>' value='2'> Tidak Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_price"."$noj4";?>' value='1'> Sangat Tidak Setuju &emsp; </br></br>
                          </td>
                        </tr>
                      <?php }else{ ?>
                        <tr>
                          <td>Silahkan membuat pertanyaan tentang Price</td>
                        </tr>
                      <?php }} ?>
                  </tbody>
                </table>
                </br>
                </br>
                </br>

                <table class="table table-index">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Pertanyaan Promotion</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no5 = 1;
                      $noj5 = 0;
                      $strSQL5 = "SELECT * FROM tb_pertanyaan_promotion WHERE id";
                      $query5 = mysqli_query ($koneksi, $strSQL5) or die ("Query Salah");
                      while ($row5 = mysqli_fetch_array($query5)){
                      $id5 = $row5 ['id'];
                      $noj5++;

                      if ($row5["p_promotion"]) { ?>
                        <tr>
                          <td align='center' valign='top'><?php echo $no5++; ?></td>
                          <td><?php echo $row5["p_promotion"]; ?></br></br>
                            <input required type='radio' name='<?php echo "j_promotion"."$noj5";?>' value='5'> Sangat Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_promotion"."$noj5";?>' value='4'> Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_promotion"."$noj5";?>' value='3'> Netral &emsp;
                            <input required type='radio' name='<?php echo "j_promotion"."$noj5";?>' value='2'> Tidak Setuju &emsp;
                            <input required type='radio' name='<?php echo "j_promotion"."$noj5";?>' value='1'> Sangat Tidak Setuju &emsp; </br></br>
                          </td>
                        </tr>
                      <?php }else{ ?>
                        <tr>
                          <td>Silahkan membuat pertanyaan tentang Promotion</td>
                        </tr>
                      <?php }} ?>
                  </tbody>
                </table>
                </br>

              <span style="color: #6c6c6c">Kritik & Saran!</span>
              <textarea maxlength="300" class="textbox2" type="text" name="kritik" placeholder="Bagaimana pelayanan kita hari ini:)"></textarea>

              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <button type="submit" name="tambah" class="btn btn-primary float-right"><i class="fas fa-check"></i> Selesai</button>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </form>

  <!-- /.content-wrapper -->
  <footer class="main-footer2">
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020.</strong> All rights reserved.
  </footer>


  <script src="assets/js/jquery.js"></script> 
  <script src="assets/js/popper.js"></script> 
  <script src="assets/js/bootstrap.js"></script>
</body>
</html>