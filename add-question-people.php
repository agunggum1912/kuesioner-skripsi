<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }


  if (isset($_POST['tambah'])) {
    $tambah1 = trim($_POST['tambahpertanyaan1']);
    $tambah2 = trim($_POST['tambahpertanyaan2']);
    $tambah3 = trim($_POST['tambahpertanyaan3']);
    $tambah4 = trim($_POST['tambahpertanyaan4']);
    $tambah5 = trim($_POST['tambahpertanyaan5']);

    if (empty($tambah1) && empty($tambah2) && empty($tambah3) && empty($tambah4) && empty($tambah5)) {
      echo "<script>alert('Data tidak ada yang terisi!');history.go(-1)</script>";
    }elseif (strlen($tambah1) > 300) {
      echo "<script>alert('No 1 lebih dari 300 Karakter!');history.go(-1)</script>";
    }elseif (strlen($tambah2) > 300) {
      echo "<script>alert('No 2 lebih dari 300 Karakter!');history.go(-1)</script>";
    }elseif (strlen($tambah3) > 300) {
      echo "<script>alert('No 3 lebih dari 300 Karakter!');history.go(-1)</script>";
    }elseif (strlen($tambah4) > 300) {
      echo "<script>alert('No 4 lebih dari 300 Karakter!');history.go(-1)</script>";
    }elseif (strlen($tambah5) > 300) {
      echo "<script>alert('No 5 lebih dari 300 Karakter!');history.go(-1)</script>";
    }else{
      if (!empty($tambah1)) {
        $query1 = "INSERT INTO tb_pertanyaan_people(id,p_people,time_steam) VALUES (NULL,'$tambah1',current_timestamp())";
        $hasil1 = mysqli_query($koneksi, $query1);
      }
      
      if (!empty($tambah2)) {
        $query2 = "INSERT INTO tb_pertanyaan_people(id,p_people,time_steam) VALUES (NULL,'$tambah2',current_timestamp())";
        $hasil2 = mysqli_query($koneksi, $query2);  
      }
      
      if (!empty($tambah3)) {
        $query3 = "INSERT INTO tb_pertanyaan_people(id,p_people,time_steam) VALUES (NULL,'$tambah3',current_timestamp())";
        $hasil3 = mysqli_query($koneksi, $query3);        
      }

      if (!empty($tambah4)) {
        $query4 = "INSERT INTO tb_pertanyaan_people(id,p_people,time_steam) VALUES (NULL,'$tambah4',current_timestamp())";
        $hasil4 = mysqli_query($koneksi, $query4);  
      }

      if (!empty($tambah5)) {
        $query5 = "INSERT INTO tb_pertanyaan_people(id,p_people,time_steam) VALUES (NULL,'$tambah5',current_timestamp())";
        $hasil5 = mysqli_query($koneksi, $query5);          
      }  

      echo "<script>alert('Telah berhasil di tambahkan.') ;window.location='addquestion.php'; </script>";
    }
  }
?>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <?php
            $strSQL = "SELECT tb_user.id, tb_user.username, tb_user.nama, tb_user.foto FROM tb_user WHERE username='$_SESSION[userlogin]' ";
            $query = mysqli_query ($koneksi, $strSQL) or die ("query salah");
            while ($row = mysqli_fetch_array($query)){
            $id = $row ['id'];

            echo $row["username"];

          ?> 
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="reportresult.php" class="brand-link">
      <img src="gambar/logom10.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Mitra10 Q-Big</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row ['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="setting.php" class="d-block">
            <?php echo $row["nama"]; }?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="reportresult.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Report Result
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deleteuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="edituserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Manage Question
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="addquestion.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="deletequestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Delete Question</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="editquestion.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit Question</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="lockscreen.php?username=<?php echo $_SESSION['userlogin']; ?>" class="nav-link">
              <font style="color: #ed1c24;"><i class="nav-icon fas fa-user-lock"></i></font>
              <p>
                LockScreen
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Question</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="reportresult.php">Home</a></li>
              <li class="breadcrumb-item"><a href="addquestion.php">Manage Question</a></li>
              <li class="breadcrumb-item"><a href="addquestion.php">Add Question</a></li>
              <li class="breadcrumb-item active">Add Question People</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">    
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Add Question People</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <form action="" method="post" name="tambah">
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table-question m-0">
                      <thead>
                      <tr>
                        <th><center>No</center></th>
                        <th>Question</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td width="6.5%"><center>1</center></td>
                        <td><input type="text" name="tambahpertanyaan1" class="form-control input_user border-list" value=""></td>
                      </tr>
                      <tr>
                        <td width="6.5%"><center>2</center></td>
                        <td><input type="text" name="tambahpertanyaan2" class="form-control input_user border-list" value=""></td>
                      </tr>
                      <tr>
                        <td width="6.5%"><center>3</center></td>
                        <td><input type="text" name="tambahpertanyaan3" class="form-control input_user border-list" value=""></td>
                      </tr>
                      <tr>
                        <td width="6.5%"><center>4</center></td>
                        <td><input type="text" name="tambahpertanyaan4" class="form-control input_user border-list" value=""></td>
                      </tr>
                      <tr>
                        <td width="6.5%"><center>5</center></td>
                        <td><input type="text" name="tambahpertanyaan5" class="form-control input_user border-list" value=""></td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                  <button  onclick="return confirm('Pertanyaan yang di buat apakah sudah benar?')"  type="submit" name="tambah" class="btn btn-primary float-right"><i class='font-setting-4 fas fa-plus'></i> Add</button>
                  <a href="addquestion.php" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp;</a>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <!-- <a href="http://adminlte.io">AdminLTE.io</a> -->.</strong>
    All rights reserved.
  </footer>
</div>


<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
