-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Jul 2020 pada 13.59
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kuesioner`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_judul`
--

CREATE TABLE `tb_judul` (
  `id` int(10) NOT NULL,
  `judul` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_judul`
--

INSERT INTO `tb_judul` (`id`, `judul`) VALUES
(1, 'People'),
(2, 'Place'),
(3, 'Price'),
(4, 'Produk'),
(5, 'Promotion');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kritik`
--

CREATE TABLE `tb_kritik` (
  `id` int(10) NOT NULL,
  `kritik` varchar(300) DEFAULT NULL,
  `no_qb` int(10) NOT NULL,
  `time_steam` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kritik`
--

INSERT INTO `tb_kritik` (`id`, `kritik`, `no_qb`, `time_steam`) VALUES
(1, 'Puasss berbelanja di mitra10.', 200000, '2020-06-21 14:07:30'),
(2, 'Baguss tingkatkan lagiiii', 200001, '2020-06-21 14:10:03'),
(3, 'event discount besar donnggg', 200002, '2020-06-21 14:14:08'),
(4, 'cukup', 200003, '2020-06-21 14:14:48'),
(5, 'Sangat Bagusss', 200004, '2020-06-21 14:15:17'),
(6, 'Brand nya bagus bagusss', 200005, '2020-06-21 14:15:54'),
(7, 'Setuju semuaaa', 200006, '2020-06-21 14:16:31'),
(8, 'Netral bossss', 200011, '2020-06-23 08:40:06'),
(9, 'Lama pengambilan keramiknyaa', 200007, '2020-06-21 14:17:47'),
(10, 'sejauh ini semuanya bagus', 200008, '2020-06-21 14:18:56'),
(11, 'SPG/SPB nya jutekkkk', 200009, '2020-06-21 14:19:32'),
(12, 'ok kok', 200010, '2020-06-23 07:57:45'),
(13, 'bismillah', 200012, '2020-06-23 08:59:58'),
(14, 'Testtt', 200200, '2020-06-23 12:40:27'),
(15, 'bismillah semoga lancar', 200020, '2020-06-23 14:07:09');

--
-- Trigger `tb_kritik`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kritik` AFTER INSERT ON `tb_kritik` FOR EACH ROW BEGIN
	INSERT INTO tb_place (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_last_login`
--

CREATE TABLE `tb_last_login` (
  `id_admin` int(10) NOT NULL,
  `time_steam` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_last_login`
--

INSERT INTO `tb_last_login` (`id_admin`, `time_steam`) VALUES
(10, '2020-06-26'),
(11, '2020-06-30'),
(12, '2020-06-30'),
(13, '2020-06-30'),
(14, '2020-06-30'),
(15, '2020-06-30'),
(17, '2020-06-30'),
(18, '2020-06-30'),
(19, '2020-07-01'),
(20, '2020-07-01'),
(21, '2020-07-01'),
(22, '2020-07-01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_people`
--

CREATE TABLE `tb_people` (
  `id` int(10) NOT NULL,
  `j_people1` int(1) NOT NULL,
  `j_people2` int(1) NOT NULL,
  `j_people3` int(1) NOT NULL,
  `j_people4` int(1) NOT NULL,
  `j_people5` int(1) NOT NULL,
  `p_people6` int(1) NOT NULL,
  `p_people7` int(1) NOT NULL,
  `p_people8` int(1) NOT NULL,
  `p_people9` int(1) NOT NULL,
  `p_people10` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_people`
--

INSERT INTO `tb_people` (`id`, `j_people1`, `j_people2`, `j_people3`, `j_people4`, `j_people5`, `p_people6`, `p_people7`, `p_people8`, `p_people9`, `p_people10`) VALUES
(1, 4, 3, 4, 5, 0, 0, 0, 0, 0, 0),
(2, 5, 4, 3, 4, 0, 0, 0, 0, 0, 0),
(3, 5, 5, 4, 5, 0, 0, 0, 0, 0, 0),
(4, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(5, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(6, 4, 5, 5, 4, 0, 0, 0, 0, 0, 0),
(7, 3, 4, 3, 3, 0, 0, 0, 0, 0, 0),
(8, 5, 3, 3, 5, 0, 0, 0, 0, 0, 0),
(9, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(10, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(11, 2, 3, 4, 5, 0, 0, 0, 0, 0, 0),
(12, 5, 4, 5, 5, 0, 0, 0, 0, 0, 0),
(13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(15, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_people`
--

CREATE TABLE `tb_pertanyaan_people` (
  `id` int(10) NOT NULL,
  `p_people` varchar(300) DEFAULT NULL,
  `time_steam` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pertanyaan_people`
--

INSERT INTO `tb_pertanyaan_people` (`id`, `p_people`, `time_steam`) VALUES
(43, 'Customer memperoleh pelayanan yang ramah di Mitra10 Q-Big.', '2020-06-29 14:33:43'),
(44, 'Pelayanan yang sopan dan menyenangkan.', '2020-06-29 14:33:43'),
(45, 'Pelayanan yang cepat dan sigap.', '2020-06-29 14:33:43'),
(46, 'Karyawan mampu memberikan informasi produk dengan jelas.', '2020-06-29 14:33:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_place`
--

CREATE TABLE `tb_pertanyaan_place` (
  `id` int(10) NOT NULL,
  `p_place` varchar(300) DEFAULT NULL,
  `time_steam` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pertanyaan_place`
--

INSERT INTO `tb_pertanyaan_place` (`id`, `p_place`, `time_steam`) VALUES
(40, 'Mitra10 Q-Big mudah di jangkau pengunjung.', '2020-06-29 15:54:14'),
(41, 'Mitra10 Q-big menyediakan tempat parkir yang luas dan terjamin keamanannya.', '2020-06-29 15:54:14'),
(42, 'Lokasi Mitra10 Q-big terletak di lingkungan yang aman.', '2020-06-29 15:54:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_price`
--

CREATE TABLE `tb_pertanyaan_price` (
  `id` int(10) NOT NULL,
  `p_price` varchar(300) DEFAULT NULL,
  `time_steam` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pertanyaan_price`
--

INSERT INTO `tb_pertanyaan_price` (`id`, `p_price`, `time_steam`) VALUES
(1, 'Mitra10 mempunyai produk dengan harga yang terjangkau oleh semua kalangan.', '2020-06-17 06:41:24'),
(2, 'Harga dengan kualitas produk-produk yang ada di Mitra10 sesuai dengan yang di tawarkan.', '2020-06-17 06:41:30'),
(3, 'Harga produk-produk Mitra10 mampu bersaing dan sesuai dengan kemampuan atau daya beli masyarakat.', '2020-06-17 06:41:35'),
(4, 'Mitra10 memberikan kemudahan pembayaran secara tunai atau bisa menggunakan kartu kredit/debet.', '2020-06-17 06:41:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_produk`
--

CREATE TABLE `tb_pertanyaan_produk` (
  `id` int(11) NOT NULL,
  `p_produk` varchar(300) NOT NULL,
  `time_steam` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pertanyaan_produk`
--

INSERT INTO `tb_pertanyaan_produk` (`id`, `p_produk`, `time_steam`) VALUES
(1, 'Variasi produk yang disediakan oleh mitra10 lengkap dan beragam.', '2020-06-25 07:29:50'),
(2, 'Kualitas produk yang ada di mitra10 baik dan bagus.', '2020-06-25 07:29:50'),
(3, 'Mitra10 menyediakan produk-produk dengan beragam merk terkenal.', '2020-06-25 07:29:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_promotion`
--

CREATE TABLE `tb_pertanyaan_promotion` (
  `id` int(10) NOT NULL,
  `p_promotion` varchar(300) DEFAULT NULL,
  `time_steam` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pertanyaan_promotion`
--

INSERT INTO `tb_pertanyaan_promotion` (`id`, `p_promotion`, `time_steam`) VALUES
(1, 'Produk-produk Mitra10 melakukan promosi penjualan melalui discount, member deal, cashback, dan lain- lain.', '2020-06-17 07:00:05'),
(2, 'Mitra10 sering melakukan event untuk memberikan discount atau cashback terhadap produk-produk.', '2020-06-17 07:00:05'),
(3, 'Karyawan Mitra10 memberikan pelayanan dengan service terbaik kepada pelanggan.', '2020-06-17 07:00:05'),
(4, 'Mitra10 melakukan promosi melalui media elektronik dan banner di jalan utama.', '2020-06-17 07:00:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_place`
--

CREATE TABLE `tb_place` (
  `id` int(10) NOT NULL,
  `j_place1` int(1) NOT NULL,
  `j_place2` int(1) NOT NULL,
  `j_place3` int(1) NOT NULL,
  `j_place4` int(1) NOT NULL,
  `j_place5` int(1) NOT NULL,
  `j_place6` int(1) NOT NULL,
  `j_place7` int(1) NOT NULL,
  `j_place8` int(1) NOT NULL,
  `j_place9` int(1) NOT NULL,
  `j_place10` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_place`
--

INSERT INTO `tb_place` (`id`, `j_place1`, `j_place2`, `j_place3`, `j_place4`, `j_place5`, `j_place6`, `j_place7`, `j_place8`, `j_place9`, `j_place10`) VALUES
(1, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0),
(2, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0),
(3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0),
(4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0),
(5, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0),
(6, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0),
(7, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0),
(8, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0),
(9, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0),
(10, 4, 2, 5, 0, 0, 0, 0, 0, 0, 0),
(11, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0),
(12, 5, 4, 5, 0, 0, 0, 0, 0, 0, 0),
(13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0),
(15, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0);

--
-- Trigger `tb_place`
--
DELIMITER $$
CREATE TRIGGER `after_insert_place` AFTER INSERT ON `tb_place` FOR EACH ROW BEGIN
	INSERT INTO tb_price (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_price`
--

CREATE TABLE `tb_price` (
  `id` int(10) NOT NULL,
  `j_price1` int(1) NOT NULL,
  `j_price2` int(1) NOT NULL,
  `j_price3` int(1) NOT NULL,
  `j_price4` int(1) NOT NULL,
  `j_price5` int(1) NOT NULL,
  `j_price6` int(1) NOT NULL,
  `j_price7` int(1) NOT NULL,
  `j_price8` int(1) NOT NULL,
  `j_price9` int(1) NOT NULL,
  `j_price10` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_price`
--

INSERT INTO `tb_price` (`id`, `j_price1`, `j_price2`, `j_price3`, `j_price4`, `j_price5`, `j_price6`, `j_price7`, `j_price8`, `j_price9`, `j_price10`) VALUES
(1, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(2, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0),
(4, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(5, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(6, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(7, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0),
(8, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0),
(9, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(10, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(11, 5, 4, 4, 5, 0, 0, 0, 0, 0, 0),
(12, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(15, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0);

--
-- Trigger `tb_price`
--
DELIMITER $$
CREATE TRIGGER `after_insert_price` AFTER INSERT ON `tb_price` FOR EACH ROW BEGIN
	INSERT INTO tb_produk (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id` int(10) NOT NULL,
  `j_produk1` int(1) NOT NULL,
  `j_produk2` int(1) NOT NULL,
  `j_produk3` int(1) NOT NULL,
  `j_produk4` int(1) NOT NULL,
  `j_produk5` int(1) NOT NULL,
  `j_produk6` int(1) NOT NULL,
  `j_produk7` int(1) NOT NULL,
  `j_produk8` int(1) NOT NULL,
  `j_produk9` int(1) NOT NULL,
  `j_produk10` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_produk`
--

INSERT INTO `tb_produk` (`id`, `j_produk1`, `j_produk2`, `j_produk3`, `j_produk4`, `j_produk5`, `j_produk6`, `j_produk7`, `j_produk8`, `j_produk9`, `j_produk10`) VALUES
(1, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(2, 4, 5, 4, 5, 0, 0, 0, 0, 0, 0),
(3, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0),
(4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(5, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(6, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(7, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0),
(8, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0),
(9, 4, 5, 4, 5, 0, 0, 0, 0, 0, 0),
(10, 4, 2, 4, 5, 0, 0, 0, 0, 0, 0),
(11, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(12, 5, 4, 4, 5, 0, 0, 0, 0, 0, 0),
(13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(15, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0);

--
-- Trigger `tb_produk`
--
DELIMITER $$
CREATE TRIGGER `after_insert_produk` AFTER INSERT ON `tb_produk` FOR EACH ROW BEGIN
	INSERT INTO tb_promotion (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_promotion`
--

CREATE TABLE `tb_promotion` (
  `id` int(10) NOT NULL,
  `j_promotion1` int(1) NOT NULL,
  `j_promotion2` int(1) NOT NULL,
  `j_promotion3` int(1) NOT NULL,
  `j_promotion4` int(1) NOT NULL,
  `j_promotion5` int(1) NOT NULL,
  `j_promotion6` int(1) NOT NULL,
  `j_promotion7` int(1) NOT NULL,
  `j_promotion8` int(1) NOT NULL,
  `j_promotion9` int(1) NOT NULL,
  `j_promotion10` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_promotion`
--

INSERT INTO `tb_promotion` (`id`, `j_promotion1`, `j_promotion2`, `j_promotion3`, `j_promotion4`, `j_promotion5`, `j_promotion6`, `j_promotion7`, `j_promotion8`, `j_promotion9`, `j_promotion10`) VALUES
(1, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(2, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(3, 5, 5, 1, 1, 0, 0, 0, 0, 0, 0),
(4, 4, 3, 3, 4, 0, 0, 0, 0, 0, 0),
(5, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(6, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(7, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0),
(8, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0),
(9, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(10, 5, 4, 4, 5, 0, 0, 0, 0, 0, 0),
(11, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(12, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0),
(13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0),
(15, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0);

--
-- Trigger `tb_promotion`
--
DELIMITER $$
CREATE TRIGGER `after_insert_promotion` AFTER INSERT ON `tb_promotion` FOR EACH ROW BEGIN
	INSERT INTO tb_people (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_update_pertanyaan`
--

CREATE TABLE `tb_update_pertanyaan` (
  `id` int(10) NOT NULL,
  `pertanyaan_sebelum` varchar(300) NOT NULL,
  `pertanyaan_sesudah` varchar(300) NOT NULL,
  `jenis_pertanyaan` varchar(9) NOT NULL,
  `time_steam` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `id_level` int(1) NOT NULL,
  `level` varchar(20) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(300) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(9) NOT NULL,
  `foto` varchar(300) NOT NULL,
  `login_terakhir` datetime DEFAULT NULL,
  `tgl_daftar` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `id_level`, `level`, `username`, `password`, `nama`, `no_hp`, `tgl_lahir`, `jenis_kelamin`, `foto`, `login_terakhir`, `tgl_daftar`) VALUES
(1, 1, 'Relationship Manager', 'agung1912', '200820e3227815ed1756a6b531e7e0d2', 'Agung Gumelar', '08999977778', '1997-12-19', 'Male', '20200629161102pas foto.png', '2020-06-05 23:36:03', '2020-06-29 16:59:14'),
(11, 1, 'Store Manager', 'gumelar1219', '200820e3227815ed1756a6b531e7e0d2', 'Gumelar Agung', '087976668888', '1997-12-19', 'Male', '20200629190403pas foto.jpg', NULL, '2020-06-29 17:06:04'),
(22, 1, 'Store Manager', 'agung1111', '200820e3227815ed1756a6b531e7e0d2', 'gumelar', '089799910000', '1997-12-19', 'Male', '20200701055946pas foto.jpg', NULL, '2020-07-01 03:59:46');

--
-- Trigger `tb_user`
--
DELIMITER $$
CREATE TRIGGER `update_login` AFTER INSERT ON `tb_user` FOR EACH ROW BEGIN
    INSERT INTO tb_last_login(id_admin,time_steam) VALUES(NEW.id,now()); 
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_judul`
--
ALTER TABLE `tb_judul`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_kritik`
--
ALTER TABLE `tb_kritik`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_qb` (`no_qb`);

--
-- Indeks untuk tabel `tb_people`
--
ALTER TABLE `tb_people`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pertanyaan_people`
--
ALTER TABLE `tb_pertanyaan_people`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pertanyaan_place`
--
ALTER TABLE `tb_pertanyaan_place`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pertanyaan_price`
--
ALTER TABLE `tb_pertanyaan_price`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pertanyaan_produk`
--
ALTER TABLE `tb_pertanyaan_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pertanyaan_promotion`
--
ALTER TABLE `tb_pertanyaan_promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_place`
--
ALTER TABLE `tb_place`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_price`
--
ALTER TABLE `tb_price`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_promotion`
--
ALTER TABLE `tb_promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_update_pertanyaan`
--
ALTER TABLE `tb_update_pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_judul`
--
ALTER TABLE `tb_judul`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_kritik`
--
ALTER TABLE `tb_kritik`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tb_people`
--
ALTER TABLE `tb_people`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tb_pertanyaan_people`
--
ALTER TABLE `tb_pertanyaan_people`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT untuk tabel `tb_pertanyaan_place`
--
ALTER TABLE `tb_pertanyaan_place`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `tb_pertanyaan_price`
--
ALTER TABLE `tb_pertanyaan_price`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tb_pertanyaan_produk`
--
ALTER TABLE `tb_pertanyaan_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_pertanyaan_promotion`
--
ALTER TABLE `tb_pertanyaan_promotion`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_place`
--
ALTER TABLE `tb_place`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `tb_price`
--
ALTER TABLE `tb_price`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `tb_promotion`
--
ALTER TABLE `tb_promotion`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `tb_update_pertanyaan`
--
ALTER TABLE `tb_update_pertanyaan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
