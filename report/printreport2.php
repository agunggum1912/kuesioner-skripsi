<?php
$vendor_file=dirname(__FILE__).'/html2pdf/vendor/autoload.php'; 
require_once $vendor_file;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
// use Spipu\Html2Pdf\src;
try {
    ob_start();
    include dirname(__FILE__).'/print/contoh.php';
    $content = ob_get_clean();
    $html2pdf = new Html2Pdf('P', 'A4', 'en');
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->writeHTML($content);
    $html2pdf->output('example00.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();
    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}