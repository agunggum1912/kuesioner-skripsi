<?php
include('../koneksi.php');
session_start();
require('fpdf.php');



//A4 width : 219mm
//default margin : 10mm each side
//writable horizontal : 219-(10*2)=189mm

$pdf = new FPDF('P','mm','A4');

$pdf->AddPage();

//Image( file name , x position , y position , width [optional] , height [optional] )
$pdf->Image('LogoMitra10.png',60,30,89);


//set font to arial, bold, 14pt
$pdf->SetFont('Arial','B',14);



//Cell(width , height , text , border , end line , [align] )

$pdf->Cell(130 ,5,'TUTORIALSWB DAN TUTORPHPID',0,0);
$pdf->Cell(59 ,5,'CHECK FASILITAS',0,1);//end of line

//set font to arial, regular, 12pt
$pdf->SetFont('Arial','',12);

$pdf->Cell(130 ,5,'Jl. Tani, Mamampang',0,0);
$pdf->Cell(59 ,5,'',0,1);//end of line
$pdf->Cell(130 ,5,'Garassi, Tinggimoncong, 92174',0,0);


$query= "SELECT * FROM tb_user";
$query2 = mysqli_query($koneksi, $query) or die ("query user salah");
    while($row=mysqli_fetch_array($query2)){


$pdf->Cell(25 ,5,'Tanggal  :',0,0);
$pdf->Cell(34 ,5,$row['nama'],0,1);//end of line

$pdf->Cell(130 ,5,'Nomor Telp (+62) 81524737292',0,0);
$pdf->Cell(25 ,5,'Nomor    :',0,0);
$pdf->Cell(34 ,5,$row['id'],0,1);//end of line

$pdf->Cell(130 ,5,'Fax (0411) 11223344',0,0);
$pdf->Cell(25 ,5,'Teknisi   :',0,0);
$pdf->Cell(34 ,5,$row['username'],0,1);//end of line


//make a dummy empty cell as a vertical spacer
$pdf->Cell(189 ,10,'',0,1);//end of line

//billing address
$pdf->SetFont('Arial','B',12);
$pdf->Cell(100 ,5,'DETAILS LAPORAN CHECK',0,1);//end of line
$pdf->Cell(100 ,5,'',0,1);//end of line


$pdf->SetFont('Arial','B',10);
$pdf->Cell(45 ,5,'Rumah Department',0,0);
$pdf->SetFont('Arial','I',10);
$pdf->Cell(90 ,5, $row['level'],0,1);
$pdf->Cell(100 ,2,'',0,1);//end of line


$pdf->SetFont('Arial','B',10);
$pdf->Cell(45 ,5,'Lokasi',0,0);
$pdf->SetFont('Arial','I',10);
$pdf->Cell(90 ,5,$row['level'],0,1);
$pdf->Cell(100 ,2,'',0,1);//end of line


$pdf->SetFont('Arial','B',10);
$pdf->Cell(45 ,5,'Nama Teknisi',0,0);
$pdf->SetFont('Arial','I',10);
$pdf->Cell(90 ,5,$row['nama'],0,1);
$pdf->Cell(100 ,2,'',0,1);//end of line


$pdf->SetFont('Arial','B',10);
$pdf->Cell(45 ,5,'Penanggung Jawab',0,0);
$pdf->SetFont('Arial','I',10);
$pdf->Cell(90 ,5,$row['level'],0,1);
$pdf->Cell(100 ,2,'',0,1);//end of line


$pdf->SetFont('Arial','B',10);
$pdf->Cell(45 ,5,'Nama Fasilitas',0,0);
$pdf->SetFont('Arial','I',10);
$pdf->Cell(90 ,5,$row['username'],0,1);
$pdf->Cell(100 ,2,'',0,1);//end of line



$pdf->Cell(130 ,4,'PEMILIK RUMAH',0,0);
$pdf->Cell(59 ,5,'APARAT KEAMANAN',0,1);//end of line


}

$pdf->Output("Laporan Check.pdf","I");
?>