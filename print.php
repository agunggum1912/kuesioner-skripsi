<!DOCTYPE html>
<html>
<head>
	<title></title>
<?php
	include 'koneksi.php';
	include 'algoritma.php';

	// mengaktifkan session
	session_start();
	if (!isset($_SESSION['userlogin'])) {
	// if($_SESSION['status'] != "login") {
		echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
	}

	$sql1 = "SELECT no_qb, time_steam FROM tb_kritik";
	$qry1 = mysqli_query ($koneksi, $sql1) or die ("query kritik salah");
                  
	$sqlpromotion = "SELECT * FROM tb_promotion";
	$qrypromotion = mysqli_query($koneksi, $sqlpromotion) or die ("query promotion salah");

	$sqlpeople = "SELECT * FROM tb_people";
	$qrypeople = mysqli_query($koneksi, $sqlpeople) or die ("query people salah");

	$sqlproduk = "SELECT * FROM tb_produk";
	$qryproduk = mysqli_query($koneksi, $sqlproduk) or die ("query produk salah");

	$sqlprice = "SELECT * FROM tb_price";
	$qryprice = mysqli_query($koneksi, $sqlprice) or die ("query price salah");

	$sqlplace = "SELECT * FROM tb_place";
	$qryplace = mysqli_query($koneksi, $sqlplace) or die ("query place salah");
	$no = 0;

?>

<style type="text/css">
	body{
		font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
		-webkit-print-color-adjust: exact;
	}
	.logo {
		height: 150px;
		width: 300px;
		display: block;
	    margin-left: auto;
    	margin-right: auto;
	}

	.judul {
		display: block;
		text-align: center;
		border-top: 5px double #000;
		border-bottom: 5px double #000;
		padding: 10px 0px;
	}

	.tabel {
		border-collapse: collapse;
		width: 100%;
		font-family: sans-serif;
	}

	.tabel thead, th {
		background-color: #00aeef;
		color: #fff;
		border: 2px solid #000;
		padding: 15px 10px;
	}

	.tabel td {
		border: 1px solid #000;
		padding: 5px 10px;
		text-align: center;
	}

	.tabel tr:nth-child(odd){
		background-color: #d7d7d7;
	}

</style>
</head>
<body>
	<img class="logo" src="gambar/logomitra10.svg">
 	<span style="display: block; margin: 0; text-align: center;">Jl. BSD Raya Utama, BSD City, Tangerang</span>
 	<span style="display: block; margin: 0;  text-align: center;">Telp (021) 80634588</span></br>

 	<h1 class="judul">REPORT KUESIONER</h1>

	<table class="tabel">
		<thead>
			<tr>
				<th>No</th>
				<th>No QB</th>
				<th>Score</th>
				<th>Presentation</th>
				<th>Date Time</th>
			</tr>
		</thead>
		<tbody>
		<?php
			error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			while ($data1 = mysqli_fetch_array($qry1)) {
				$no++;
				$datapromotion = mysqli_fetch_array($qrypromotion);
				$datapeople = mysqli_fetch_array($qrypeople);
				$dataproduk = mysqli_fetch_array($qryproduk);
				$dataprice = mysqli_fetch_array($qryprice);
				$dataplace = mysqli_fetch_array($qryplace);
				$noqb = $data1['no_qb'];
				$time = $data1['time_steam'];
				$promotion1 = $datapromotion['j_promotion1'];
				$promotion2 = $datapromotion['j_promotion2'];
				$promotion3 = $datapromotion['j_promotion3'];
			    $promotion4 = $datapromotion['j_promotion4'];
			    $promotion5 = $datapromotion['j_promotion5'];
			    $promotion6 = $datapromotion['j_promotion6'];
			    $promotion7 = $datapromotion['j_promotion7'];
			    $promotion8 = $datapromotion['j_promotion8'];
		        $promotion9 = $datapromotion['j_promotion9'];
		        $promotion10 = $datapromotion['j_promotion10'];
		        $people1 = $datapeople['j_people1'];
		        $people2 = $datapeople['j_people2'];
		        $people3 = $datapeople['j_people3'];
				$people4 = $datapeople['j_people4'];
				$people5 = $datapeople['j_people5'];
				$people6 = $datapeople['j_people6'];
				$people7 = $datapeople['j_people7'];
				$people8 = $datapeople['j_people8'];
				$people9 = $datapeople['j_people9'];
				$people10 = $datapeople['j_people10'];
				$produk1 = $dataproduk['j_produk1'];
				$produk2 = $dataproduk['j_produk2'];
				$produk3 = $dataproduk['j_produk3'];
				$produk4 = $dataproduk['j_produk4'];
				$produk5 = $dataproduk['j_produk5'];
				$produk6 = $dataproduk['j_produk6'];
				$produk7 = $dataproduk['j_produk7'];
				$produk8 = $dataproduk['j_produk8'];
				$produk9 = $dataproduk['j_produk9'];
				$produk10 = $dataproduk['j_produk10'];
			    $price1 = $dataprice['j_price1'];
			    $price2 = $dataprice['j_price2'];
			    $price3 = $dataprice['j_price3'];
			    $price4 = $dataprice['j_price4'];
			    $price5 = $dataprice['j_price5'];
			    $price6 = $dataprice['j_price6'];
			    $price7 = $dataprice['j_price7'];
			    $price8 = $dataprice['j_price8'];
			    $price9 = $dataprice['j_price9'];
			    $price10 = $dataprice['j_price10'];
		        $place1 = $dataplace['j_place1'];
		        $place2 = $dataplace['j_place2'];
		        $place3 = $dataplace['j_place3'];
		        $place4 = $dataplace['j_place4'];
		        $place5 = $dataplace['j_place5'];
		        $place6 = $dataplace['j_place6'];
		        $place7 = $dataplace['j_place7'];
		        $place8 = $dataplace['j_place8'];
		        $place9 = $dataplace['j_place9'];
		        $place10 = $dataplace['j_place10'];

				if ($promotion1 != 0) {
					$npromotion1 = 1;
				}

				if ($promotion2 != 0) {
					$npromotion2 = 1;
				}

				if ($promotion3 != 0) {
					$npromotion3 = 1;
				}

				if ($promotion4 != 0) {
					$npromotion4 = 1;
				}

				if ($promotion5 != 0) {
					$npromotion5 = 1;
				}

				if ($promotion6 != 0) {
					$npromotion6 = 1;
				}

				if ($promotion7 != 0) {
					$npromotion7 = 1;
				}

				if ($promotion8 != 0) {
					$npromotion8 = 1;
				}

				if ($promotion9 != 0) {
					$npromotion9 = 1;
				}

				if ($promotion10 != 0) {
					$npromotion10 = 1;
				}

				if ($people1 != 0) {
					$npeople1 = 1;
				}

				if ($people2 != 0) {
					$npeople2 = 1;
				}

				if ($people3 != 0) {
					$npeople3 = 1;
				}

				if ($people4 != 0) {
					$npeople4 = 1;
				}

				if ($people5 != 0) {
					$npeople5 = 1;
				}

				if ($people6 != 0) {
					$npeople6 = 1;
				}

				if ($people7 != 0) {
					$npeople7 = 1;
				}

				if ($people8 != 0) {
					$npeople8 = 1;
				}

				if ($people9 != 0) {
					$npeople9 = 1;
				}

				if ($people10 != 0) {
					$npeople10 = 1;
				}

				if ($produk1 != 0) {
					$nproduk1 = 1;
				}

				if ($produk2 != 0) {
					$nproduk2 = 1;
				}

				if ($produk3 != 0) {
					$nproduk3 = 1;
				}

				if ($produk4 != 0) {
					$nproduk4 = 1;
				}

				if ($produk5 != 0) {
					$nproduk5 = 1;
				}

				if ($produk6 != 0) {
					$nproduk6 = 1;
				}

				if ($produk7 != 0) {
					$nproduk7 = 1;
				}

				if ($produk8 != 0) {
					$nproduk8 = 1;
				}

				if ($produk9 != 0) {
					$nproduk9 = 1;
				}

				if ($produk10 != 0) {
					$nproduk10 = 1;
				}

				if ($price1 != 0) {
					$nprice1 = 1;
				}

				if ($price2 != 0) {
					$nprice2 = 1;
				}

				if ($price3 != 0) {
					$nprice3 = 1;
				}

				if ($price4 != 0) {
					$nprice4 = 1;
				}

				if ($price5 != 0) {
					$nprice5 = 1;
				}

				if ($price6 != 0) {
					$nprice6 = 1;
				}

				if ($price7 != 0) {
					$nprice7 = 1;
				}

				if ($price8 != 0) {
					$nprice8 = 1;
				}

				if ($price9 != 0) {
					$nprice9 = 1;
				}

				if ($price10 != 0) {
					$nprice10 = 1;
				}

				if ($place1 != 0) {
					$nplace1 = 1;
				}

				if ($place2 != 0) {
					$nplace2 = 1;
				}

				if ($place3 != 0) {
					$nplace3 = 1;
				}

				if ($place4 != 0) {
					$nplace4 = 1;
				}

				if ($place5 != 0) {
					$nplace5 = 1;
				}

				if ($place6 != 0) {
					$nplace6 = 1;
				}

				if ($place7 != 0) {
					$nplace7 = 1;
				}

				if ($place8 != 0) {
					$nplace8 = 1;
				}

				if ($place9 != 0) {
					$nplace9 = 1;
				}

				if ($place10 != 0) {
					$nplace10 = 1;
				}

				$sum =$promotion1 + $promotion2 + $promotion3 + $promotion4 + $promotion5 + $promotion6 + $promotion7 + $promotion8 + $promotion9 + $promotion10 + $people1 + $people2 + $people3 + $people4 + $people5 + $people6 + $people7 + $people8 + $people9 + $people10 + $produk1 + $produk2 + $produk3 + $produk4 + $produk5 + $produk6 + $produk7 + $produk8 + $produk9 + $produk10 + $price1 + $price2 + $price3 + $price4 + $price5 + $price6 + $price7 + $price8 + $price9 + $price10 + $place1 + $place2 + $place3 + $place4 + $place5 + $place6 + $place7 + $place8 + $place9 + $place10;

				$nilaipembagi = $npromotion1 + $npromotion2 + $npromotion3 + $npromotion4 + $npromotion5 + $npromotion6 + $npromotion7 + $npromotion8 + $npromotion9 + $npromotion10 + $npeople1 + $npeople2 + $npeople3 + $npeople4 + $npeople5 + $npeople6 + $npeople7 + $npeople8 + $npeople9 + $npeople10 + $nproduk1 + $nproduk2 + $nproduk3 + $nproduk4 + $nproduk5 + $nproduk6 + $nproduk7 + $nproduk8 + $nproduk9 + $nproduk10 + $nprice1 + $nprice2 + $nprice3 + $nprice4 + $nprice5 + $nprice6 + $nprice7 + $nprice8 + $nprice9 + $nprice10 + $nplace + $nplace1 + $nplace2 + $nplace3 + $nplace4 + $nplace5 + $nplace6 + $nplace7 + $nplace8 + $nplace9 + $nplace10;
				$hasil = $sum / $nilaipembagi;

			    $hasilpersen = $hasil / 5 * 100;
		        $konversipersen = number_format($hasilpersen,1,",",".");

	            $hasilkonversi = number_format($hasil,1,",",".");
  
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $noqb; ?></td>
				<td><?php echo $hasilkonversi; ?></td>
				<td><?php echo $konversipersen;?>%</td>
				<td><?php echo $time; ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
 
	<script>
		window.print();
	</script>
 
</body>
</html>